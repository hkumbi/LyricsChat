//
//  FriendsViewButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import Foundation

protocol FriendsViewButtonDelegate : AnyObject {
    
    func buttonPressed(for type : FriendsViewButtonType)
}

enum FriendsViewButtonType {
    case back, add, friendsTab, requestsTab
}
