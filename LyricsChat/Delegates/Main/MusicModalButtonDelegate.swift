//
//  MusicModalButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-14.
//

import Foundation


protocol MusicModalButtonDelegate : AnyObject {
    
    func buttonPressed(for type : MusicModalButtonType)
}

enum MusicModalButtonType {
    case nicki, drake, ariana, kanye, cancel
}
