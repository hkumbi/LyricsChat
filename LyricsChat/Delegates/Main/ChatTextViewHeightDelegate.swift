//
//  ChatTextViewHeightDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-08.
//

import Foundation
import UIKit


protocol ChatTextViewHeightDelegate : AnyObject {
    
    func updateContentHeight(_ height : CGFloat)
}
