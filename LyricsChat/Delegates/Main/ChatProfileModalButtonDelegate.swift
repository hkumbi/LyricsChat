//
//  ChatProfileModalButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import Foundation


protocol ChatProfileModalButtonDelegate : AnyObject {
    
    func closeButtonPressed()
}
