//
//  NewFriendsCellButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-18.
//

import Foundation


protocol NewFriendsCellButtonDelegate : AnyObject {
    
    func sendRequest(_ id : UUID)
}
