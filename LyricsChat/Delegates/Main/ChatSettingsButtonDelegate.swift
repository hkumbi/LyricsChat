//
//  ChatSettingsButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-15.
//

import Foundation


protocol ChatSettingsButtonDelegate : AnyObject {
    
    func buttonPressed(for type : ChatSettingsButtonType)
}

enum ChatSettingsButtonType {
    case cancel, deleteAllMessages, unfriend
}
