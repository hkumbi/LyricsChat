//
//  ChatButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-08.
//

import Foundation


protocol ChatButtonDelegate : AnyObject {
    
    func buttonPressed(for type : ChatButtonType)
}

enum ChatButtonType {
    case back, settings, profile, music, send
}
