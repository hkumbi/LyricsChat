//
//  RequestCellButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import Foundation


protocol RequestCellButtonDelegate : AnyObject {
    
    func buttonPressed(for type : RequestCellButtonType, _ id : UUID)
}

enum RequestCellButtonType {
    case add, delete
}
