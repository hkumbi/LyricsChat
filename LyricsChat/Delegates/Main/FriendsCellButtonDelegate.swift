//
//  FriendsCellButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import Foundation


protocol FriendsCellButtonDelegate : AnyObject {
    
    func unfriendButtonPressed(_ id : UUID)
}
