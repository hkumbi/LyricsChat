//
//  PinTextFieldDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import Foundation


protocol PinTextFieldDelegate : AnyObject {
    
    func deleteBackward(_ textField : PinTextField)
    func textDidChange(_ textField : PinTextField)
}
