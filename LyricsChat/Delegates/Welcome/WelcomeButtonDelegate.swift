//
//  WelcomeButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import Foundation


protocol WelcomeButtonDelegate : AnyObject {
    
    func buttonPressed(for type : WelcomeButtonType)
}

enum WelcomeButtonType {
    case signIn, signUp
}
