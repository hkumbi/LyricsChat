//
//  CompleteProfileButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import Foundation


protocol CompleteProfileButtonDelegate : AnyObject {
    
    func buttonPressed(for type : CompleteProfileButtonType)
}

enum CompleteProfileButtonType {
    case skip, selectPhoto, save
}
