//
//  SelectPhotoButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-06.
//

import Foundation

protocol SelectPhotoButtonDelegate : AnyObject {
    
    func buttonPressed(for type : SelectPhotoButtonType)
}

enum SelectPhotoButtonType {
    case cancel, save, permission, allowAccess
}
