//
//  VerificationPinButtonDelegate.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import Foundation

protocol VerificationPinButtonDelegate : AnyObject {
    
    func resendCodeButtonPressed()
    func backButtonPressed()
}
