//
//  CustomFonts.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import Foundation
import UIKit

extension UIFont {
    
    static let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
    
}
