//
//  LineSpacing.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-06.
//

import Foundation
import UIKit

extension UILabel {
    
    func addLineSpacing(_ spacing : CGFloat = 2, width : CGFloat = 0) -> CGFloat? {
        
        guard let text = text, let font = font else { return nil }
        
        let attributedString = NSMutableAttributedString(string: text)
        
        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.lineSpacing = spacing
        paragraphStyle.alignment = textAlignment
        
        attributedString.addAttribute(
            .paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length)
        )
        
        attributedText = attributedString
        
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: paragraphStyle], context: nil)
        
        return ceil(boundingBox.height)
    }

}
