//
//  SingleValueInset.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-07.
//

import Foundation
import UIKit


extension UIEdgeInsets {
    
    static func from(_ value : CGFloat) -> UIEdgeInsets {
        UIEdgeInsets(top: value, left: value, bottom: value, right: value)
    }
    
}
