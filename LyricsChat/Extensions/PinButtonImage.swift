//
//  PinButtonImage.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import Foundation
import UIKit


extension UIButton {
    
    func pinImageView() {
        
        guard let imageView = imageView else { return }
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.leftAnchor.constraint(equalTo: leftAnchor),
            imageView.rightAnchor.constraint(equalTo: rightAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
