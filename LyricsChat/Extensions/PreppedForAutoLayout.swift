//
//  PreppedForAutoLayout.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-03.
//

import Foundation
import UIKit


extension UIView {
    
    static func viewPreppedForAutoLayout() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}


extension UIButton {
    
    static func preppedForAutoLayout() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}


extension UILabel {
    
    static func preppedForAutoLayout() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
}


extension UITextField {
    
    static func preppedForAutoLayout() -> UITextField {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
}


extension UIImageView {
    
    static func preppedForAutoLayout() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}


extension BottomBorderTextField {
    
    static func bottomPreppedForAutoLayout() -> BottomBorderTextField {
        let bottomTextField = BottomBorderTextField()
        bottomTextField.translatesAutoresizingMaskIntoConstraints = false
        return bottomTextField
    }
}


extension BackgroundButton {
    
    static func backgroundPreppedForAutoLayout() -> BackgroundButton {
        let backgroundButton = BackgroundButton()
        backgroundButton.translatesAutoresizingMaskIntoConstraints = false
        return backgroundButton
    }
}


extension PinTextField {
    
    static func pinPreppedForAutoLayout() -> PinTextField {
        let pinTextField = PinTextField()
        pinTextField.translatesAutoresizingMaskIntoConstraints = false
        return pinTextField
    }
}


extension IconButton {
    
    static func iconPreppedForAutoLayout() -> IconButton {
        let iconButton = IconButton()
        iconButton.translatesAutoresizingMaskIntoConstraints = false
        return iconButton
    }
}


extension DefaultCollectionView {
    
    static func defaultPreppedForAutoLayout() -> DefaultCollectionView {
        let defaultCollection = DefaultCollectionView()
        defaultCollection.translatesAutoresizingMaskIntoConstraints = false
        return defaultCollection
    }
}


extension DefaultTableView {
    
    static func defaultPreppedForAutoLayout() -> DefaultTableView {
        let defaultTable = DefaultTableView()
        defaultTable.translatesAutoresizingMaskIntoConstraints = false
        return defaultTable
    }
}


extension UICollectionView {
    
    static func preppedForAutoLayout() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }
}


extension UITextView {
    
    static func preppedForAutoLayout() -> UITextView {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }
}


extension UISearchBar {
    
    static func preppedForAutoLayout() -> UISearchBar {
        let searchBar = UISearchBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchBar
    }
}
