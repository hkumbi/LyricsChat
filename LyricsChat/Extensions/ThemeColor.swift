//
//  ThemeColor.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-03.
//

import Foundation
import UIKit


extension UIColor {
    
    static let customGreen = UIColor(red: 76/255, green: 147/255, blue: 76/255, alpha: 1)
    
}
