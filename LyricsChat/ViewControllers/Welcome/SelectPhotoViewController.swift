//
//  SelectPhotoViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import UIKit
import PhotosUI

class SelectPhotoViewController: UIViewController {
    
    private typealias DataSouce = UICollectionViewDiffableDataSource<SectionModel, PHAsset>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, PHAsset>
    
    private let contentView = SelectPhotoView()
    
    private lazy var dataSource = makeDataSource()
    private lazy var imageManager = PHCachingImageManager()
    private let cellID = "CellIdentifier"
    private var requestImageSize : CGSize!
    private let requestOption = PHImageRequestOptions()
    private var isLimitedAccess : Bool = false
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        requestImageSize = CGSize(width: PhotoGalleryCollectionViewCell.width*2, height: PhotoGalleryCollectionViewCell.height*2)
        
        requestOption.isNetworkAccessAllowed = true
        requestOption.resizeMode = .fast
        
        contentView.buttonDelegate = self
        contentView.changeViewStatus(.notDetermined)
        
        setUpCollectionView()
        requestPhotos()
    }
    
    private func setUpCollectionView() {
        
        let collection = contentView.collectionView
        
        collection.delegate = self
        collection.dataSource = dataSource
        collection.collectionViewLayout = makeLayout()
        collection.allowsSelection = true
        collection.register(PhotoGalleryCollectionViewCell.self, forCellWithReuseIdentifier: cellID)
    }
    
    private func makeLayout() -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = PhotoGalleryCollectionViewCell.size
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        return layout
    }
    
    private func makeDataSource() -> DataSouce {
        let source = DataSouce(collectionView: contentView.collectionView) {
            [unowned self]collection, indexPath, item in
            
            let cell = collection.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! PhotoGalleryCollectionViewCell
            
            cell.identiifer = item.localIdentifier
            
            imageManager.requestImage(for: item, targetSize: requestImageSize, contentMode: .aspectFill, options: requestOption){ image, _ in
                
                if cell.identiifer == item.localIdentifier {
                    cell.image = image
                }
            }
            
            return cell
        }
        
        return source
    }
    
    private func requestPhotos() {
        
        let status = PHPhotoLibrary.authorizationStatus(for: .readWrite)
                
        guard status == .notDetermined else {
            handleStatus(status)
            return
        }

        PHPhotoLibrary.requestAuthorization(for: .readWrite){
            [weak self]newStatus in
                        
            self?.handleStatus(newStatus)
        }
    }
    
    private func handleStatus(_ status : PHAuthorizationStatus) {
        DispatchQueue.main.async {[weak self] in
            self?.contentView.changeViewStatus(status)
        }
        
        if status == .authorized || status == .limited {
            PHPhotoLibrary.shared().register(self)
            isLimitedAccess = status == .limited
            handleAssets()
        }
    }
    
    private func handleAssets() {
        
        let options = PHFetchOptions()
        
        options.sortDescriptors = [NSSortDescriptor(key: "modificationDate", ascending: false)]
        
        let fetchAssets = PHAsset.fetchAssets(with: .image, options: options)
        var assets : [PHAsset] = []
        
        fetchAssets.enumerateObjects({asset, _, _ in
            assets.append(asset)
        })
        
        imageManager.startCachingImages(
            for: assets,
            targetSize: requestImageSize,
            contentMode: .aspectFill,
            options: requestOption)
        
        var snap = Snap()
        snap.appendSections([.main])
        snap.appendItems(assets)
        
        DispatchQueue.main.async {[weak self] in
            self?.dataSource.apply(snap)
        }
    }

}


extension SelectPhotoViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoGalleryCollectionViewCell
        
        contentView.selectedImage = cell.image
    }
    
}


extension SelectPhotoViewController : SelectPhotoButtonDelegate {
    
    func buttonPressed(for type: SelectPhotoButtonType) {
        
        switch type {
        case .cancel:
            print("Cancel")
            
        case .save:
            print("Save")
            
        case .permission:
            if isLimitedAccess {
                permissionLimited()
            } else {
                permissionFull()
            }
            
        case .allowAccess:
            openSettings()
        }
    }
    
}


extension SelectPhotoViewController {
    
    private func permissionLimited() {

        let actionSheet = UIAlertController(
            title: "Manage Access",
            message: "Select more photos or go to settings and allow access to all photos",
            preferredStyle: .alert)
        
        let limitedAction = UIAlertAction(
            title: "Select More Photos",
            style: .default) { [unowned self]_ in
            PHPhotoLibrary.shared().presentLimitedLibraryPicker(from: self)
        }
        
        let manageAction = UIAlertAction(
            title: "Allow access to all photos",
            style: .default) { [unowned self]_ in
            openSettings()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        actionSheet.addAction(limitedAction)
        actionSheet.addAction(manageAction)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true)
    }
    
    private func permissionFull() {
        
        let actionSheet = UIAlertController(
            title: "Manage Access",
            message: "Go to settings to limit access to photos",
            preferredStyle: .alert)
        
        let manageAction = UIAlertAction(
            title: "Limit Access",
            style: .destructive) { [unowned self]_ in
            openSettings()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        actionSheet.addAction(manageAction)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true)
    }
    
    private func openSettings() {
        
        guard
            let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url)
        else { return }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
}


extension SelectPhotoViewController : PHPhotoLibraryChangeObserver {
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        handleAssets()
    }
    
}
