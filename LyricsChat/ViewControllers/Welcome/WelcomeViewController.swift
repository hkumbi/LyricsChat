//
//  WelcomeViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-03.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    private let contentView = WelcomeView()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
    }

}


extension WelcomeViewController : WelcomeButtonDelegate {
    
    func buttonPressed(for type: WelcomeButtonType) {
        
        switch type {
        case .signIn:
            print("Sign In")
        case .signUp:
            print("Sign Up")
        }
    }
        
}
