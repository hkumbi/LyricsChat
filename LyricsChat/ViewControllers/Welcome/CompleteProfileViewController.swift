//
//  CompleteProfileViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import UIKit

class CompleteProfileViewController: UIViewController {
    
    private let contenView = CompleteProfileView()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contenView)
        
        contenView.buttonDelegate = self
    }
        
}


extension CompleteProfileViewController : CompleteProfileButtonDelegate {
    
    func buttonPressed(for type: CompleteProfileButtonType) {
        
        switch type {
        case .skip:
            print("Skip")
        case .selectPhoto:
            print("Select Photo")
        case .save:
            print("Save")
        }
    }
    
}
