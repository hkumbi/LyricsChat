//
//  VerificationPinViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-04.
//

import UIKit

class VerificationPinViewController: UIViewController {
    
    private let contentView = VerificationPinView()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.setTextFieldDelegates(self)
        contentView.setPinDelegates(self)
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.firstDigitTextField.becomeFirstResponder()
    }
    

}


extension VerificationPinViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let trimmedText = newText.trimmingCharacters(in: .decimalDigits)
        
        return newText.count <= 1 && trimmedText.isEmpty
    }
        
}


extension VerificationPinViewController : PinTextFieldDelegate {
    
    func deleteBackward(_ textField: PinTextField) {
        
        guard textField.text == "" else { return }
                
        if textField == contentView.secondDigitTextField {
            contentView.firstDigitTextField.becomeFirstResponder()
        } else if textField == contentView.thirdDigitTextField {
            contentView.secondDigitTextField.becomeFirstResponder()
        } else if textField == contentView.fourthDigitTextField {
            contentView.thirdDigitTextField.becomeFirstResponder()
        } else if textField == contentView.fifthDigitTextField {
            contentView.fourthDigitTextField.becomeFirstResponder()
        } else if textField == contentView.sixthDigitTextField {
            contentView.fifthDigitTextField.becomeFirstResponder()
        }
    }
    
    func textDidChange(_ textField: PinTextField) {
        
        guard textField.text?.count == 1 else { return }
            
        if textField == contentView.firstDigitTextField {
            contentView.secondDigitTextField.becomeFirstResponder()
        } else if textField == contentView.secondDigitTextField {
            contentView.thirdDigitTextField.becomeFirstResponder()
        } else if textField == contentView.thirdDigitTextField {
            contentView.fourthDigitTextField.becomeFirstResponder()
        } else if textField == contentView.fourthDigitTextField {
            contentView.fifthDigitTextField.becomeFirstResponder()
        } else if textField == contentView.fifthDigitTextField {
            contentView.sixthDigitTextField.becomeFirstResponder()
        }
        
        checkIfPinIsComplete()
    }
        
}


extension VerificationPinViewController : VerificationPinButtonDelegate {
    
    func backButtonPressed() {
        print("Back button pressed")
    }
    
    func resendCodeButtonPressed() {
        print("Resend code pressed")
    }
    
}


extension VerificationPinViewController {
    
    func checkIfPinIsComplete() {
        
        let first = contentView.firstDigitTextField.text ?? ""
        let second = contentView.secondDigitTextField.text ?? ""
        let third = contentView.thirdDigitTextField.text ?? ""
        let fourth = contentView.fourthDigitTextField.text ?? ""
        let fifth = contentView.fifthDigitTextField.text ?? ""
        let sixth = contentView.sixthDigitTextField.text ?? ""
        
        let pin = first + second + third + fourth + fifth + sixth
        
        guard pin.count == 6 else { return }
        
        verifyPin()
    }
    
    func verifyPin() {
        print("Verify Pin")
    }
    
}
