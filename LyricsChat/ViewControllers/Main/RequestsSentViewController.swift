//
//  RequestsSentViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import UIKit

class RequestsSentViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<SectionModel, RequestModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, RequestModel>
    
    private let contentView = RequestSentView()
    
    private lazy var dataSource = makeDataSource()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        setUpCollectionView()
        makeData()
    }
    
    private func setUpCollectionView() {
        
        let collection = contentView.collectionView
        
        collection.delegate = self
        collection.dataSource = dataSource
        collection.collectionViewLayout = makeLayout()
        collection.register(MyRequestCollectionViewCell.self, forCellWithReuseIdentifier: MyRequestCollectionViewCell.id)
    }
    
    private func makeLayout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        
        layout.itemSize = MyRequestCollectionViewCell.size
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        
        return layout
    }
    
    private func makeDataSource() -> DataSource {
        
        let source = DataSource(collectionView: contentView.collectionView){
            [unowned self]collectionView, indexPath, item in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyRequestCollectionViewCell.id, for: indexPath)
            
            return cell
        }
        
        return source
    }
    
    private func makeData() {
        
        var snap = Snap()
        snap.appendSections([.main])
        snap.appendItems([RequestModel(), RequestModel(), RequestModel()])
        dataSource.apply(snap)
    }

}


extension RequestsSentViewController : UICollectionViewDelegate {
    
}
