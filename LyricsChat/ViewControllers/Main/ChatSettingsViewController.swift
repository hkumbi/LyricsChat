//
//  ChatSettingsViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-14.
//

import UIKit

class ChatSettingsViewController: UIViewController {
    
    private let contentView = ChatSettingsModalView()
    
    private var closeHeight : CGFloat = 0
    
    var modalHeight : CGFloat {
        get { contentView.modalView.height }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.modalView.buttonDelegate = self
        
        setUpGesture()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        closeHeight = modalHeight*0.25
        contentView.modalView.bottomSafeAreaConstant = view.safeAreaInsets.bottom
        contentView.modalBottomConstant = modalHeight
        animateOpen()
    }
    
    private func setUpGesture() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        contentView.modalView.addGestureRecognizer(pan)
    }
    
    @objc private func handlePan(_ pan : UIPanGestureRecognizer) {
        
        let transY = pan.translation(in: nil).y
        let veloY = pan.velocity(in: nil).y
        
        switch pan.state {
        case .changed:
            if transY >= 0 {
                let dimming = (transY/modalHeight)*ChatSettingsModalView.maxDimmingAlpha
                contentView.modalBottomConstant = transY
                contentView.dimmingAlpha = ChatSettingsModalView.maxDimmingAlpha-dimming
            }
            
        case .ended, .failed, .cancelled:
            if transY >= closeHeight || veloY >= 800 {
                animateClose()
            } else {
                animateOpen()
            }
            
        default:
            break
        }
    }
}

extension ChatSettingsViewController : ChatSettingsButtonDelegate {
    func buttonPressed(for type: ChatSettingsButtonType) {
        
        switch type {
        case .cancel:
            animateClose()
            
        case .deleteAllMessages:
            print("Delete All Messages")
            
        case .unfriend:
            print("Unfriend")
        }
    }
    
    
}


extension ChatSettingsViewController {
    
    private func animateOpen() {
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = 0
            contentView.dimmingAlpha = ChatSettingsModalView.maxDimmingAlpha
            view.layoutIfNeeded()
        }
    }
    
    private func animateClose() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = modalHeight
            contentView.dimmingAlpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            dismiss(animated: false)
        }
    }
    
}
