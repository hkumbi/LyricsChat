//
//  MusicModalViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-13.
//

import UIKit

class MusicModalViewController: UIViewController {
    
    private let contentView = MusicModalView()
    
    private var closeHeight : CGFloat = 0
    
    private var modalHeight : CGFloat {
        get { contentView.modalView.fullHeight }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.modalView.buttonDelegate = self
        
        setUpPan()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        closeHeight = modalHeight*0.25
        contentView.modalView.bottomSafeAreaConstant = view.safeAreaInsets.bottom
        contentView.modalBottomConstant = modalHeight
        animateOpen()
    }

    private func setUpPan() {
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        contentView.modalView.addGestureRecognizer(pan)
    }
    
    @objc private func handlePan(_ pan : UIPanGestureRecognizer) {
        
        let transY = pan.translation(in: nil).y
        let veloY = pan.velocity(in: nil).y
        
        switch pan.state {
        case .changed:
            if transY >= 0 {
                let dimming = (transY/modalHeight)*MusicModalView.maxDimmingAlpha
                contentView.modalBottomConstant = transY
                contentView.dimmingAlpha = MusicModalView.maxDimmingAlpha-dimming
            }
            
        case .ended, .failed, .cancelled:
            if transY >= closeHeight || veloY >= 800 {
                animateClose()
            } else {
                animateOpen()
            }
            
        default:
            break
        }
    }
    
}


extension MusicModalViewController {
    
    private func animateOpen() {
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = 0
            contentView.dimmingAlpha = MusicModalView.maxDimmingAlpha
            view.layoutIfNeeded()
        }
    }
    
    private func animateClose() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = modalHeight
            contentView.dimmingAlpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            dismiss(animated: false)
        }
    }
    
}


extension MusicModalViewController : MusicModalButtonDelegate {
    
    func buttonPressed(for type: MusicModalButtonType) {
        
        switch type {
        case .nicki:
            print("Nicki Minaj")
        case .drake:
            print("Drake")
        case .ariana:
            print("Ariana Grande")
        case .kanye:
            print("Kanye West")
        case .cancel:
            animateClose()
        }
    }
    
}
