//
//  MessagesViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-06.
//

import UIKit

class MessagesViewController: UIViewController {
    
    typealias DataSource = UITableViewDiffableDataSource<SectionModel, MessageModel>
    typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, MessageModel>
    
    private let contentView = MessagesView()
    private lazy var dataSource = makeDataSource()
    private let cellID = "CellIdentifier"
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        setUpTableView()
        makeData()
    }
    
    private func setUpTableView() {
        
        let table = contentView.tableView
        
        table.delegate = self
        table.dataSource = dataSource
        table.rowHeight = MessageTableViewCell.height
        table.register(MessageTableViewCell.self, forCellReuseIdentifier: cellID)
    }

    private func makeDataSource() -> DataSource {
        
        let source = DataSource(tableView: contentView.tableView){
            [unowned self]tableView, indexPath, item in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
            
            return cell
        }
        return source
    }
    
    private func makeData() {
        
        var snap = Snap()
        snap.appendSections([.main])
        snap.appendItems([MessageModel(), MessageModel(), MessageModel()])
        dataSource.apply(snap)
    }
    
}

extension MessagesViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let muteAction = UIContextualAction(style: .normal, title: nil){
            action, sourceview, completionHandler in
            print("Mute")
            completionHandler(true)
        }
        
        let deleteAction = UIContextualAction(style: .normal, title: nil){
            action, sourceview, completionHandler in
            print("Delete")
            completionHandler(true)
        }
        
        let config = UISwipeActionsConfiguration(actions: [deleteAction, muteAction])

        
        muteAction.image = UIImage(systemName: "bell.slash", withConfiguration: UIImage.SymbolConfiguration(weight: .semibold))
        muteAction.backgroundColor = UIColor(red: 65/255, green: 105/255, blue: 225/255, alpha: 1)

        deleteAction.image = UIImage(systemName: "trash", withConfiguration: UIImage.SymbolConfiguration(weight: .semibold))
        deleteAction.backgroundColor = UIColor(red: 220/255, green: 20/255, blue: 60/255, alpha: 1)
                
        config.performsFirstActionWithFullSwipe = false
        
        return config
        
    }
    
}
