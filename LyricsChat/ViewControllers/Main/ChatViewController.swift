//
//  ChatViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-07.
//

import UIKit

class ChatViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<SectionModel, ChatBaseModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, ChatBaseModel>
    
    private struct Cells {
        static let myChat = "MyChatCellID"
        static let myProfile = "MyProfileCellID"
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }
    
    private let contentView = ChatView()
    private lazy var dataSource = makeDataSource()
        

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.textViewDelegate = self
        
        contentView.sendButtonEnabled = false
        contentView.musicButtonEnabled = true
        
        setUpCollectionView()
        makeData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.collectionView.contentInset.bottom = contentView.createCommentHeight
    }
    
    private func setUpCollectionView() {
        
        let collection = contentView.collectionView
        let layout = UICollectionViewFlowLayout()
        
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        
        collection.delegate = self
        collection.dataSource = dataSource
        collection.collectionViewLayout = layout
        collection.register(MyChatCollectionViewCell.self, forCellWithReuseIdentifier: Cells.myChat)
        collection.register(MyChatProfilePhotoCollectionViewCell.self, forCellWithReuseIdentifier: Cells.myProfile)
    }
    
    
    private func makeDataSource() -> DataSource {
        
        let source = DataSource(collectionView: contentView.collectionView){
            [unowned self]collectionView, indexPath, item in
            
            if let item = item as? ChatModel  {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cells.myChat, for: indexPath) as! MyChatCollectionViewCell
                
                cell.text = item.text
                cell.isMyChat = item.isMyChat
                
                return cell

            } else if let item = item as? ChatProfileModel {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cells.myProfile, for: indexPath) as! MyChatProfilePhotoCollectionViewCell
                
                cell.isMyChat = item.isMyChat
                                
                return cell
            }
                
            fatalError("Not using storyboards")
        }
        
        return source
    }
    
    private func makeData() {
        
        var snap = Snap()
        
        snap.appendSections([.main])
        
        for _ in 0..<2 {
            
            for _ in 0..<3 {
                snap.appendItems([ChatModel()])
            }
            
            snap.appendItems([ChatProfileModel()], toSection: .main)
            
            for _ in 0..<3 {
                let model = ChatModel()
                model.isMyChat = true
                snap.appendItems([model])
            }

            let model = ChatProfileModel()
            model.isMyChat = true
            snap.appendItems([model])
        }
        
        for _ in 0..<3 {
            snap.appendItems([ChatModel()])
        }
                
        dataSource.apply(snap, animatingDifferences: false)
    }
}

extension ChatViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView.panGestureRecognizer.velocity(in: view).y <= -2000 {
            print("Hello")
            //view.endEditing(true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let item = dataSource.itemIdentifier(for: indexPath)!
        
        if let item = item as? ChatModel {
            
            let height = String.getLabelHeight(
                text: item.text,
                font: MyChatCollectionViewCell.textFont,
                width: MyChatCollectionViewCell.maxTextWidth)
            let spacing = MyChatCollectionViewCell.spacing
            
            return CGSize(width: UIScreen.main.bounds.width, height: height+spacing)
            
        } else if let _ = item as? ChatProfileModel {
            
            return MyChatProfilePhotoCollectionViewCell.size
        }
        
        return .zero
    }
        
    func collectionView(_ collectionView: UICollectionView, shouldSpringLoadItemAt indexPath: IndexPath, with context: UISpringLoadedInteractionContext) -> Bool {
        false
    }
}


extension ChatViewController : UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {

        contentView.sendButtonEnabled = !textView.text.isEmpty
        contentView.musicButtonEnabled = textView.text.isEmpty
    }
    
}

extension ChatViewController : ChatButtonDelegate {
    
    func buttonPressed(for type: ChatButtonType) {
        
        view.endEditing(true)

        switch type {
        case .back:
            print("Back")
            
        case .settings:
            let settingsVC = ChatSettingsViewController()
            settingsVC.modalPresentationStyle = .overCurrentContext
            present(settingsVC, animated: false)
            
        case .profile:
            let profileVC = ChatProfileModalViewController()
            profileVC.modalPresentationStyle = .overCurrentContext
            present(profileVC, animated: false)
            
        case .music:
            let musicModalVC = MusicModalViewController()
            musicModalVC.modalPresentationStyle = .overCurrentContext
            present(musicModalVC, animated: false)
            
        case .send:
            print("Send")
        }
    }
    
}


extension ChatViewController {
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {
        print("Will Hide")
        guard let userInfo = notification.userInfo else { return }

        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! CGFloat
        let curveRaw = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let option = UIView.AnimationOptions(rawValue: curveRaw)

        UIView.animate(withDuration: duration, delay: 0, options: option){
            [unowned self] in
            
            contentView.bottomConstant = 0
            contentView.collectionView.contentInset.bottom = 0
            view.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        print("Will Show")
        guard let userInfo = notification.userInfo else { return }

        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! CGFloat
        let curveRaw = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let option = UIView.AnimationOptions(rawValue: curveRaw)
        let constant = -keyboardFrame.height + view.safeAreaInsets.bottom
        
        let commentHeight = contentView.createCommentHeight + keyboardFrame.height - view.safeAreaInsets.bottom
        contentView.collectionView.contentInset.bottom = commentHeight

        UIView.animate(withDuration: duration, delay: 0, options: option){
            [unowned self] in
            
            let offset = contentView.collectionView.contentSize.height + contentView.collectionView.contentInset.bottom - contentView.collectionView.bounds.height
            contentView.collectionView.contentOffset = CGPoint(x: 0, y: offset)
            contentView.bottomConstant = constant
            view.layoutIfNeeded()
        }
        
    }
    
}


