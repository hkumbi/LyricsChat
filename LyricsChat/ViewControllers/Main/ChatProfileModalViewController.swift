//
//  ChatProfileModalViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-15.
//

import UIKit

class ChatProfileModalViewController: UIViewController {

    private let contentView = ChatProfileModalView()
    
    private var closeHeight : CGFloat = 0

    var modalHeight : CGFloat {
        get { contentView.modalView.height }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.modalView.buttonDelegate = self
        
        setUpGesture()
        
        contentView.modalView.setBioText("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        closeHeight = modalHeight*0.25
        contentView.modalView.bottomSafeAreaConstant = view.safeAreaInsets.bottom
        contentView.modalBottomConstant = modalHeight
        animateOpen()
    }

    private func setUpGesture() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        contentView.modalView.addGestureRecognizer(pan)
    }
    
    @objc private func handlePan(_ pan : UIPanGestureRecognizer){
        
        let transY = pan.translation(in: nil).y
        let veloY = pan.velocity(in: nil).y
        
        switch pan.state {
        case .changed:
            if transY >= 0 {
                let dimming = (transY/modalHeight)*ChatProfileModalView.maxDimmingAlpha
                contentView.modalBottomConstant = transY
                contentView.dimmingAlpha = ChatProfileModalView.maxDimmingAlpha-dimming
            }
            
        case .ended, .failed, .cancelled:
            if transY >= closeHeight || veloY >= 800 {
                animateClose()
            } else {
                animateOpen()
            }
            
        default:
            break
        }
    }

}


extension ChatProfileModalViewController : ChatProfileModalButtonDelegate {
    
    func closeButtonPressed() {
        animateClose()
    }

}


extension ChatProfileModalViewController {
    
    private func animateOpen() {
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = 0
            contentView.dimmingAlpha = ChatProfileModalView.maxDimmingAlpha
            view.layoutIfNeeded()
        }
    }
    
    private func animateClose() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = modalHeight
            contentView.dimmingAlpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            dismiss(animated: false)
        }
    }
    
}
