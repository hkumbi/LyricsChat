//
//  AddNewFriendsViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import UIKit

class AddNewFriendsViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<SectionModel, UserModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, UserModel>
    
    private let contentView = AddNewFriendsView()
    
    private lazy var dataSource = makeDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.searchDelegate = self
        
        setUpCollection()
        makeData()
    }
    
    private func setUpCollection() {
        let collection = contentView.collectionView
        
        collection.delegate = self
        collection.dataSource = dataSource
        collection.collectionViewLayout = makeLayout()
        collection.register(NewFriendCollectionViewCell.self, forCellWithReuseIdentifier: NewFriendCollectionViewCell.id)
    }
    
    private func makeLayout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = NewFriendCollectionViewCell.size
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        return layout
    }
    
    private func makeDataSource() -> DataSource {
        
        let source = DataSource(collectionView: contentView.collectionView){
            [unowned self] collectionView, indexPath, item in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NewFriendCollectionViewCell.id, for: indexPath) as! NewFriendCollectionViewCell
            
            cell.id = item.id
            cell.buttonDelegate = self
            
            return cell
        }
        
        return source
    }
    
    private func makeData() {
        
        var snap = Snap()
        snap.appendSections([.main])
        snap.appendItems([UserModel(), UserModel(), UserModel(), UserModel()])
        dataSource.apply(snap)
    }

}

extension AddNewFriendsViewController : UICollectionViewDelegate {
    
}


extension AddNewFriendsViewController : UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        contentView.showCancelButton()
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        contentView.hideCancelButton()
        return true
    }
    
}


extension AddNewFriendsViewController : NewFriendsCellButtonDelegate {
    
    func sendRequest(_ id: UUID) {
        print("Send Request: \(id)")
    }
    
}
