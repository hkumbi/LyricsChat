//
//  FriendsViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import UIKit

class FriendsViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<SectionModel, UserModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, UserModel>
    
    
    private let contentView = FriendsView()
    private lazy var friendsDataSource = makeFriendsDataSource()
    private lazy var requestDataSource = makeRequestsDataSource()
    private var scrollingProgramatically : Bool = false
    
    private var firstCollection : DefaultCollectionView {
        get { contentView.scrollView.firstCollection }
    }
    
    private var secondCollection : DefaultCollectionView {
        get { contentView.scrollView.secondCollection }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.scrollView.delegate = self
        
        setUpCollection()
        setUpCollectionLayouts()
        makeData()
        
        setTabColors(0)
    }
        
    private func setTabColors(_ index : Int) {
        contentView.friendsTabColor = index == 0 ? .black : .darkGray
        contentView.requestsTabColor = index == 1 ? .black : .darkGray
    }
    
    private func setUpCollection() {
        
        firstCollection.delegate = self
        firstCollection.dataSource = friendsDataSource
        firstCollection.register(FriendsCollectionViewCell.self, forCellWithReuseIdentifier: FriendsCollectionViewCell.id)
        
        secondCollection.delegate = self
        secondCollection.dataSource = requestDataSource
        secondCollection.register(RequestCollectionViewCell.self, forCellWithReuseIdentifier: RequestCollectionViewCell.id)
    }
    
    private func setUpCollectionLayouts() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = FriendsCollectionViewCell.size
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        firstCollection.collectionViewLayout = layout

        let secondlayout = UICollectionViewFlowLayout()
        secondlayout.itemSize = RequestCollectionViewCell.size
        secondlayout.minimumLineSpacing = 0
        secondlayout.minimumInteritemSpacing = 0
        secondlayout.scrollDirection = .vertical
        secondCollection.collectionViewLayout = secondlayout
    }
    
    private func makeFriendsDataSource() -> DataSource {
        

        let source = DataSource(collectionView: firstCollection){
            [unowned self]collectionView, indexPath, item in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FriendsCollectionViewCell.id, for: indexPath) as! FriendsCollectionViewCell
            
            cell.cellID = item.id
            cell.buttonDelegate = self
            
            return cell
        }
        
        return source
    }
    
    private func makeRequestsDataSource() -> DataSource {
        
        let source = DataSource(collectionView: secondCollection){
            [unowned self]collectionView, indexPath, item in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RequestCollectionViewCell.id, for: indexPath) as! RequestCollectionViewCell
            
            cell.cellID = item.id
            cell.buttonDelegate = self
            
            return cell
        }
        
        return source
    }

    
    private func makeData() {
        
        var items : [UserModel] = []
        
        for _ in 0...5 {
            items.append(UserModel())
        }
        
        var friendsSnap = Snap()
        friendsSnap.appendSections([.main])
        friendsSnap.appendItems(items)
        friendsDataSource.apply(friendsSnap)
        
        var requestSnap = Snap()
        requestSnap.appendSections([.main])
        requestSnap.appendItems(items)
        requestDataSource.apply(requestSnap)
    }
    
    private func scrollScrollView(to index : Int) {
        
        let x = UIScreen.main.bounds.width*CGFloat(index)
                        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear){
            [unowned self] in

            contentView.scrollView.contentOffset.x = x
            contentView.tabLineCenterXConstraint.constant = CGFloat(index)*contentView.centerX
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            setTabColors(index)
        }
    }
}


extension FriendsViewController : UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard scrollView == contentView.scrollView else { return }
        
        contentView.lineCenterX = scrollView.contentOffset.x/UIScreen.main.bounds.width
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        guard scrollView == contentView.scrollView else { return }

        setTabColors(Int(scrollView.contentOffset.x/UIScreen.main.bounds.width))
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        guard scrollView == contentView.scrollView else { return }

        if !decelerate {
            setTabColors(Int(scrollView.contentOffset.x/UIScreen.main.bounds.width))
        }
    }
    
}


extension FriendsViewController : FriendsViewButtonDelegate {
    
    func buttonPressed(for type: FriendsViewButtonType) {
        
        switch type {
        case .back:
            print("Back")
        case .add:
            print("Add")
        case .friendsTab:
            scrollScrollView(to: 0)
        case .requestsTab:
            scrollScrollView(to: 1)
        }
    }

}


extension FriendsViewController : FriendsCellButtonDelegate {
    
    func unfriendButtonPressed(_ id: UUID) {
        
    }
    
}


extension FriendsViewController : RequestCellButtonDelegate {
    
    func buttonPressed(for type: RequestCellButtonType, _ id: UUID) {
        
        switch type {
        case .add:
            print("Add \(id)")
        case .delete:
            print("Delete \(id)")
        }
    }
    
}
