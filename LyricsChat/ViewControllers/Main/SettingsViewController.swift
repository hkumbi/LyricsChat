//
//  SettingsViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import UIKit

class SettingsViewController: UIViewController {
    
    private let contentView = SettingsView()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
    }
    

}
