//
//  SettingsChangeTextViewController.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import UIKit

class SettingsChangeTextViewController: UIViewController {
    
    private let contentView = SettingsChangeTextView()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.textViewDelegate = self
        contentView.textFieldDelegate = self
        
        contentView.showTextViewHideTextField(true)
        contentView.placeholder = "Enter New Name"
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    

}


extension SettingsChangeTextViewController : UITextFieldDelegate {
    
    
    
}


extension SettingsChangeTextViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return newText.count <= 200
    }
    
    func textViewDidChange(_ textView: UITextView) {
        contentView.textViewPlaceholderIsHidden = !textView.text.isEmpty
    }
    
}


extension SettingsChangeTextViewController {
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {

        guard let userInfo = notification.userInfo else { return }

        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! CGFloat
        let curveRaw = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let option = UIView.AnimationOptions(rawValue: curveRaw)

        UIView.animate(withDuration: duration, delay: 0, options: option){
            [unowned self] in
            
            contentView.saveBottomConstant = 0
            view.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {

        guard let userInfo = notification.userInfo else { return }

        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! CGFloat
        let curveRaw = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let option = UIView.AnimationOptions(rawValue: curveRaw)
        let constant = -keyboardFrame.height + view.safeAreaInsets.bottom - 10
        

        UIView.animate(withDuration: duration, delay: 0, options: option){
            [unowned self] in
            
            contentView.saveBottomConstant = constant
            view.layoutIfNeeded()
        }
    }
    
    
}
