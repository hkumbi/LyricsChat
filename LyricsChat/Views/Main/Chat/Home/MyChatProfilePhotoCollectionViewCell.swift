//
//  MyChatProfilePhotoCollectionViewCell.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-09.
//

import UIKit

class MyChatProfilePhotoCollectionViewCell: UICollectionViewCell {
    
    static let size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height*0.04)
    static let spacing = UIScreen.main.bounds.width*0.05
    
    private let imageView : UIImageView = .preppedForAutoLayout()
    
    private var leftContainerConstraint : NSLayoutConstraint!
    private var rightContainerConstraint : NSLayoutConstraint!
    
    var isMyChat : Bool = false {
        didSet { isMyChatUpdated() }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(imageView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        imageView.image = UIImage(named: "profile1")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = Self.size.height/2
    }
    
    private func makeConstraints() {
        
        leftContainerConstraint = imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Self.spacing)
        rightContainerConstraint = imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Self.spacing)
        
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            imageView.heightAnchor.constraint(equalTo: contentView.heightAnchor),
            imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])
    }
    
    private func isMyChatUpdated() {
        
        if isMyChat {
            leftContainerConstraint.isActive = false
            rightContainerConstraint.isActive = true
        } else {
            rightContainerConstraint.isActive = false
            leftContainerConstraint.isActive = true
        }
    }
    
}

