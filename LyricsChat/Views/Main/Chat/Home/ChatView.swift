//
//  ChatView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-07.
//

import UIKit

class ChatView: UIView {
    
    private let headerView : UIView = .viewPreppedForAutoLayout()
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let profileImageView : UIImageView = .preppedForAutoLayout()
    private let nameButton : UIButton = .preppedForAutoLayout()
    private let settingsButton : IconButton = .iconPreppedForAutoLayout()
    private let bottomView = ChatBottomView()
    private let bottomLineView : UIView = .viewPreppedForAutoLayout()
    
    let collectionView : DefaultCollectionView = .defaultPreppedForAutoLayout()
    
    private var bottomViewBottomConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : ChatButtonDelegate! {
        didSet { bottomView.buttonDelegate = buttonDelegate }
    }
    
    var textViewDelegate : UITextViewDelegate? {
        get { bottomView.textViewDelegate }
        set { bottomView.textViewDelegate = newValue }
    }
    
    var musicButtonEnabled : Bool {
        get { bottomView.musicButtonEnabled }
        set { bottomView.musicButtonEnabled = newValue }
    }
    
    var sendButtonEnabled : Bool {
        get { bottomView.sendButtonEnabled }
        set { bottomView.sendButtonEnabled = newValue }
    }
    
    var createCommentHeight : CGFloat {
        get { bottomView.height }
    }
    
    var bottomConstant : CGFloat {
        get { bottomViewBottomConstraint.constant }
        set { bottomViewBottomConstraint.constant = newValue }
    }

    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(headerView)
        self.addSubview(backButton)
        self.addSubview(profileImageView)
        self.addSubview(nameButton)
        self.addSubview(settingsButton)
        self.addSubview(bottomLineView)
        self.addSubview(collectionView)
        self.addSubview(bottomView)

        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinViewToSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
    }
    
    private func configureViews() {
        
        backButton.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.setIconColor(.black)
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        profileImageView.image = UIImage(named: "profile1")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.isUserInteractionEnabled = true
        
        nameButton.setTitle("Junior Alverado", for: .normal)
        nameButton.setTitleColorPress(.black)
        nameButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .medium))
        nameButton.addTarget(self, action: #selector(profileButtonPressed), for: .touchUpInside)
        
        settingsButton.setImage(UIImage(systemName: "ellipsis"), for: .normal)
        settingsButton.imageView?.contentMode = .scaleAspectFit
        settingsButton.pinImageView()
        settingsButton.setIconColor(.black)
        settingsButton.addTarget(self, action: #selector(settingsButtonPressed), for: .touchUpInside)
        
        bottomLineView.backgroundColor = .black.withAlphaComponent(0.2)
        
        collectionView.alwaysBounceVertical = true
    }
    
    private func setUpGesture()  {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dimmingTapped))
        tap.numberOfTapsRequired = 1
        addGestureRecognizer(tap)
        
        let profileImageTap = UITapGestureRecognizer(target: self, action: #selector(profileButtonPressed))
        profileImageTap.numberOfTapsRequired = 1
        profileImageView.addGestureRecognizer(profileImageTap)
    }
    
    private func makeConstraints() {
        
        let sideSpacing = UIScreen.main.bounds.width*0.04
        
        bottomViewBottomConstraint = bottomView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: topAnchor),
            headerView.widthAnchor.constraint(equalTo: widthAnchor),
            headerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            headerView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.08),
            
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            profileImageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.1),
            profileImageView.heightAnchor.constraint(equalTo: profileImageView.widthAnchor),
            profileImageView.leftAnchor.constraint(equalTo: backButton.rightAnchor, constant: sideSpacing),
            profileImageView.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            
            nameButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            nameButton.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: sideSpacing),
            
            settingsButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            settingsButton.heightAnchor.constraint(equalTo: settingsButton.widthAnchor),
            settingsButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            settingsButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
            
            bottomLineView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            bottomLineView.heightAnchor.constraint(equalToConstant: 1),
            bottomLineView.widthAnchor.constraint(equalTo: widthAnchor),
            bottomLineView.centerXAnchor.constraint(equalTo: centerXAnchor),

            collectionView.topAnchor.constraint(equalTo: bottomLineView.bottomAnchor),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            bottomView.centerXAnchor.constraint(equalTo: centerXAnchor),
            bottomViewBottomConstraint,
        ])
    }
    
    @objc private func dimmingTapped() {
        endEditing(true)
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.buttonPressed(for: .back)
    }
    
    @objc private func profileButtonPressed() {
        buttonDelegate.buttonPressed(for: .profile)
    }
    
    @objc private func settingsButtonPressed() {
        buttonDelegate.buttonPressed(for: .settings)
    }
    
}
