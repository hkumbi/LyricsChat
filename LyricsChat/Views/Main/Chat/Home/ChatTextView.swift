//
//  ChatTextView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-07.
//

import UIKit

class ChatTextView: UITextView {
    
    override var contentSize: CGSize {
        willSet { heightDelegate?.updateContentHeight(newValue.height) }
    }
    
    private let chatFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    
    let minimumHeight : CGFloat
    private let verticalSpacing : CGFloat
    private let textInset : CGFloat = 10
    
    weak var heightDelegate : ChatTextViewHeightDelegate!
    
    
    init() {
        
        verticalSpacing = textInset*2
        minimumHeight = String.getLabelHeight(text: "", font: chatFont) + verticalSpacing
        
        super.init(frame: .zero, textContainer: nil)
        
        configureViews()
        
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        backgroundColor = .white
        textContainerInset = .from(textInset)
        font = chatFont
        textColor = .black
        keyboardType = .twitter
        
    }
    
}
