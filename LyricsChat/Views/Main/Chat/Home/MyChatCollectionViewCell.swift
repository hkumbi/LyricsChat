//
//  MyChatCollectionViewCell.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-08.
//

import UIKit

class MyChatCollectionViewCell: UICollectionViewCell {
    
    static let spacing = UIScreen.main.bounds.width*0.05
    static let maxTextWidth = UIScreen.main.bounds.width*0.6
    static let textFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 17))
    
    private let containerView : UIView = .viewPreppedForAutoLayout()
    private let textLabel : UILabel = .preppedForAutoLayout()
    
    private var leftContainerConstraint : NSLayoutConstraint!
    private var rightContainerConstraint : NSLayoutConstraint!
    
    var isMyChat : Bool = false {
        didSet { isMyChatUpdated() }
    }

    var text : String {
        get { textLabel.text ?? "" }
        set { textLabel.text = newValue }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(containerView)
        self.contentView.addSubview(textLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        containerView.layer.borderColor = UIColor.lightGray.cgColor
        containerView.layer.cornerRadius = UIScreen.main.bounds.width*0.055
        containerView.backgroundColor = .customGreen.withAlphaComponent(0.3)
        
        textLabel.text = ""
        textLabel.numberOfLines = 0
        textLabel.textColor = .black
        textLabel.font = Self.textFont
    }
    
    private func makeConstraints() {
        
        leftContainerConstraint = containerView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Self.spacing)
        rightContainerConstraint = containerView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Self.spacing)
        
        NSLayoutConstraint.activate([
            containerView.heightAnchor.constraint(equalTo: textLabel.heightAnchor, constant: Self.spacing),
            containerView.widthAnchor.constraint(equalTo: textLabel.widthAnchor, constant: Self.spacing*2),
            containerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            textLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            textLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            textLabel.widthAnchor.constraint(lessThanOrEqualToConstant: Self.maxTextWidth),
        ])
    }
    
    private func isMyChatUpdated() {
        
        if isMyChat {
            leftContainerConstraint.isActive = false
            rightContainerConstraint.isActive = true
        } else {
            rightContainerConstraint.isActive = false
            leftContainerConstraint.isActive = true
        }
                
        containerView.layer.borderWidth = isMyChat ? 0 : 1
        containerView.backgroundColor = isMyChat ? .customGreen.withAlphaComponent(0.3) : .white
    }
    
}


