//
//  ChatBottomView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-07.
//

import UIKit

class ChatBottomView: UIView {

    private let topBorderView : UIView = .viewPreppedForAutoLayout()
    private let musicButton : IconButton = .iconPreppedForAutoLayout()
    private let chatTextView = ChatTextView()
    private let sendButton : IconButton = .iconPreppedForAutoLayout()
    
    private var heightConstraint : NSLayoutConstraint!
    private var chatTextViewHeightConstraint : NSLayoutConstraint!

    private let minimumHeight : CGFloat
    private let spacing = UIScreen.main.bounds.height*0.04
    
    unowned var buttonDelegate : ChatButtonDelegate!
    
    var textViewDelegate : UITextViewDelegate? {
        get { chatTextView.delegate }
        set { chatTextView.delegate = newValue }
    }
    
    var height : CGFloat {
        get { heightConstraint.constant }
    }
    
    var musicButtonEnabled : Bool {
        get { musicButton.isEnabled }
        set { musicButton.isEnabled = newValue }
    }
    
    var sendButtonEnabled : Bool {
        get { sendButton.isEnabled }
        set { sendButton.isEnabled = newValue }
    }
    
    
    init() {
        
        minimumHeight = chatTextView.minimumHeight + spacing
        
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(topBorderView)
        self.addSubview(musicButton)
        self.addSubview(chatTextView)
        self.addSubview(sendButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }

    private func configureViews() {
        backgroundColor = .white
        
        topBorderView.backgroundColor = .black.withAlphaComponent(0.2)
        
        musicButton.setImage(UIImage(systemName: "music.note"), for: .normal)
        musicButton.setIconColor(.black)
        musicButton.imageView?.contentMode = .scaleAspectFit
        musicButton.pinImageView()
        musicButton.addTarget(self, action: #selector(musicButtonPressed), for: .touchUpInside)
        
        chatTextView.layer.cornerRadius = chatTextView.minimumHeight/2
        chatTextView.heightDelegate = self
        chatTextView.layer.borderColor = UIColor.black.withAlphaComponent(0.2).cgColor
        chatTextView.layer.borderWidth = 1
        
        sendButton.setImage(UIImage(systemName: "paperplane"), for: .normal)
        sendButton.setIconColor(.customGreen)
        sendButton.imageView?.contentMode = .scaleAspectFit
        sendButton.pinImageView()
        sendButton.addTarget(self, action: #selector(sendButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let sideSpacing = UIScreen.main.bounds.width*0.03
        let minimumCenterY = -minimumHeight/2
        
        translatesAutoresizingMaskIntoConstraints = false
        
        chatTextViewHeightConstraint = chatTextView.heightAnchor.constraint(equalToConstant: chatTextView.minimumHeight)
        heightConstraint = heightAnchor.constraint(equalToConstant: spacing+chatTextView.minimumHeight)
        
        NSLayoutConstraint.activate([
            heightConstraint,
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            topBorderView.topAnchor.constraint(equalTo: topAnchor),
            topBorderView.widthAnchor.constraint(equalTo: widthAnchor),
            topBorderView.centerXAnchor.constraint(equalTo: centerXAnchor),
            topBorderView.heightAnchor.constraint(equalToConstant: 1),
            
            musicButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            musicButton.heightAnchor.constraint(equalTo: musicButton.widthAnchor),
            musicButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            musicButton.centerYAnchor.constraint(equalTo: bottomAnchor, constant: minimumCenterY),
            
            chatTextView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.74),
            chatTextView.centerXAnchor.constraint(equalTo: centerXAnchor),
            chatTextView.centerYAnchor.constraint(equalTo: centerYAnchor),
            chatTextViewHeightConstraint,

            sendButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            sendButton.heightAnchor.constraint(equalTo: sendButton.widthAnchor),
            sendButton.centerYAnchor.constraint(equalTo: bottomAnchor, constant: minimumCenterY),
            sendButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
        ])
    }
    
    @objc private func musicButtonPressed() {
        buttonDelegate.buttonPressed(for: .music)
    }
    
    @objc private func sendButtonPressed() {
        buttonDelegate.buttonPressed(for: .send)
    }
}


extension ChatBottomView : ChatTextViewHeightDelegate {
    
    func updateContentHeight(_ height: CGFloat) {
        
        if height > 100 {
            heightConstraint.constant = 100+spacing
            chatTextViewHeightConstraint.constant = 100
        } else {
            heightConstraint.constant = height+spacing
            chatTextViewHeightConstraint.constant = height
        }
    }
    
}
