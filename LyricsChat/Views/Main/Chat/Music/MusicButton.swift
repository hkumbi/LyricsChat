//
//  MusicButton.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-13.
//

import UIKit

class MusicButton: BackgroundButton {
    
    static let height = UIScreen.main.bounds.height*0.08
    
    private let profileImageView : UIImageView = .preppedForAutoLayout()
    private let artistNameLabel : UILabel = .preppedForAutoLayout()
    
    var artistName : String {
        get { artistNameLabel.text ?? "" }
        set { artistNameLabel.text = newValue }
    }
    
    var profileImage : UIImage? {
        get { profileImageView.image }
        set { profileImageView.image = newValue }
    }
    

    override init() {
        super.init()
        
        configureViews()
        
        self.addSubview(profileImageView)
        self.addSubview(artistNameLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
    }
    
    private func configureViews() {
        baseColor = .white
        highlightedColor = .black.withAlphaComponent(0.1)
        
        profileImageView.clipsToBounds = true
        profileImageView.contentMode = .scaleAspectFill
        
        artistNameLabel.text = "Nicki Minaj"
        artistNameLabel.textColor = .black
        artistNameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.07
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: Self.height),
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            profileImageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.7),
            profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor),
            profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            profileImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            artistNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            artistNameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: spacing),
        ])
    }

}
