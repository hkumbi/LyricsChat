//
//  MusicModalContentView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-13.
//

import UIKit

class MusicModalContentView: UIView {
    
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let subTitleLabel : UILabel = .preppedForAutoLayout()
    private let nickiButton = MusicButton()
    private let drakeButton = MusicButton()
    private let arianaButton = MusicButton()
    private let kanyeButton = MusicButton()
    private let cancelButton : UIButton = .preppedForAutoLayout()
    
    private let height : CGFloat
    private let verticalSpacing = UIScreen.main.bounds.height*0.03
    private let buttonSpacing = UIScreen.main.bounds.height*0.015
    private let subTitleWidth = UIScreen.main.bounds.width*0.8
    
    private let subTitleText = "Select and send a random lyric from\nthe following artist."
    
    private let titleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 20, weight: .semibold))
    private let subTitleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 13))
    private let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
    
    private var heightConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : MusicModalButtonDelegate!
    
    var fullHeight: CGFloat {
        get { heightConstraint.constant }
    }
    
    var bottomSafeAreaConstant : CGFloat {
        get { heightConstraint.constant - height }
        set { heightConstraint.constant = newValue + height }
    }

    
    init() {
        
        let titleHight = String.getLabelHeight(text: "", font: titleFont)
        let buttonHeight = String.getLabelHeight(text: "", font: buttonFont)
        let spacing = verticalSpacing*3 + buttonSpacing*2
        
        subTitleLabel.text = subTitleText
        subTitleLabel.font = subTitleFont
        
        let subTitleHeight = subTitleLabel.addLineSpacing()!

        
        height = MusicButton.height*4 + titleHight + subTitleHeight + buttonHeight + spacing
        
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(titleLabel)
        self.addSubview(subTitleLabel)
        self.addSubview(nickiButton)
        self.addSubview(drakeButton)
        self.addSubview(arianaButton)
        self.addSubview(kanyeButton)
        self.addSubview(cancelButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not suing storyboards")
    }

    private func configureViews() {
        backgroundColor = .white
        layer.cornerRadius = UIScreen.main.bounds.width*0.055
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        titleLabel.text = "Choose An Artist"
        titleLabel.textColor = .black
        titleLabel.font = titleFont
        
        subTitleLabel.numberOfLines = 0
        subTitleLabel.textColor = .black
        subTitleLabel.textAlignment = .center
        
        nickiButton.artistName = "Nicki Minaj"
        nickiButton.profileImage = UIImage(named: "nickiminaj")
        nickiButton.addTarget(self, action: #selector(nickiButtonPressed), for: .touchUpInside)
        
        drakeButton.artistName = "Drake"
        drakeButton.profileImage = UIImage(named: "drake")
        drakeButton.addTarget(self, action: #selector(drakeButtonPressed), for: .touchUpInside)
        
        arianaButton.artistName = "Ariana Grande"
        arianaButton.profileImage = UIImage(named: "arianagrande")
        arianaButton.addTarget(self, action: #selector(arianaButtonPressed), for: .touchUpInside)
        
        kanyeButton.artistName = "Kanye West"
        kanyeButton.profileImage = UIImage(named: "kanyewest")
        kanyeButton.addTarget(self, action: #selector(kanyeButtonPressed), for: .touchUpInside)
        
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColorPress(.black)
        cancelButton.titleLabel?.font = buttonFont
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        heightConstraint = heightAnchor.constraint(equalToConstant: height)
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            heightConstraint,
            
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: verticalSpacing),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: verticalSpacing),
            subTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            subTitleLabel.widthAnchor.constraint(equalToConstant: subTitleWidth),
            
            nickiButton.topAnchor.constraint(equalTo: subTitleLabel.bottomAnchor, constant: verticalSpacing),
            nickiButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            drakeButton.topAnchor.constraint(equalTo: nickiButton.bottomAnchor),
            drakeButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            arianaButton.topAnchor.constraint(equalTo: drakeButton.bottomAnchor),
            arianaButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            kanyeButton.topAnchor.constraint(equalTo: arianaButton.bottomAnchor),
            kanyeButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            cancelButton.topAnchor.constraint(equalTo: kanyeButton.bottomAnchor, constant: buttonSpacing),
            cancelButton.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    @objc private func nickiButtonPressed() {
        buttonDelegate.buttonPressed(for: .nicki)
    }
    
    @objc private func drakeButtonPressed() {
        buttonDelegate.buttonPressed(for: .drake)
    }
    
    @objc private func arianaButtonPressed() {
        buttonDelegate.buttonPressed(for: .ariana)
    }
    
    @objc private func kanyeButtonPressed() {
        buttonDelegate.buttonPressed(for: .kanye)
    }
    
    @objc private func cancelButtonPressed() {
        buttonDelegate.buttonPressed(for: .cancel)
    }
    
}
