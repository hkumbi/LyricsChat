//
//  ChatProfileContentModalView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-15.
//

import UIKit

class ChatProfileContentModalView: UIView {

    private let closeButton : IconButton = .iconPreppedForAutoLayout()
    private let imageView : UIImageView = .preppedForAutoLayout()
    private let nameLabel : UILabel = .preppedForAutoLayout()
    private let bioLabel : UILabel = .preppedForAutoLayout()
    
    private let minimumHeight : CGFloat
    private let minimumBioHeight : CGFloat
    private let topSpacing = UIScreen.main.bounds.height*0.03
    private let imageSize = UIScreen.main.bounds.width*0.25
    private let bioWidth = UIScreen.main.bounds.width*0.9
    private var bioHeight : CGFloat = 0
    
    private var heightConstraint : NSLayoutConstraint!
    
    private let nameFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .medium))
    private var bioFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    
    unowned var buttonDelegate : ChatProfileModalButtonDelegate!
    
    var height : CGFloat {
        get { heightConstraint.constant }
    }
    
    var bottomSafeAreaConstant : CGFloat = 0{
        didSet { updateHeight() }
    }
    
    
    init() {
        
        let nameHeight = String.getLabelHeight(text: "", font: nameFont)
        minimumBioHeight = String.getLabelHeight(text: "", font: bioFont)
        minimumHeight = imageSize + nameHeight + minimumBioHeight + topSpacing*4

        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(closeButton)
        self.addSubview(imageView)
        self.addSubview(nameLabel)
        self.addSubview(bioLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        backgroundColor = .white
        layer.cornerRadius = UIScreen.main.bounds.width*0.055
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        closeButton.setImage(UIImage(systemName: "xmark"), for: .normal)
        closeButton.setIconColor(.black)
        closeButton.imageView?.contentMode = .scaleAspectFit
        closeButton.pinImageView()
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        
        imageView.image = UIImage(named: "profile2")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = imageSize/2
        
        nameLabel.text = "Junior Alverado"
        nameLabel.textColor = .black
        nameLabel.font = nameFont
        nameLabel.textAlignment = .center
        
        bioLabel.textColor = .black
        bioLabel.numberOfLines = 0
        bioLabel.textAlignment = .center
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        heightConstraint = heightAnchor.constraint(equalToConstant: minimumHeight)
        
        NSLayoutConstraint.activate([
            heightConstraint,
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            closeButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            closeButton.heightAnchor.constraint(equalTo: closeButton.widthAnchor),
            closeButton.topAnchor.constraint(equalTo: topAnchor, constant: spacing),
            closeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            imageView.widthAnchor.constraint(equalToConstant: imageSize),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),

            nameLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: topSpacing),
            nameLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            bioLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: topSpacing),
            bioLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            bioLabel.widthAnchor.constraint(equalToConstant: bioWidth),
        ])
    }
    
    @objc private func closeButtonPressed() {
        buttonDelegate.closeButtonPressed()
    }
    
}


extension ChatProfileContentModalView {
    
    func setBioText(_ text : String?) {
        
        if let text = text {
            let newHeight = String.getLabelHeight(text: text, font: bioFont, width: bioWidth)

            bioLabel.text = text
            bioLabel.font = bioFont
            bioHeight = newHeight
            
        } else {
            bioLabel.text = "No bio"
            bioLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.italicSystemFont(ofSize: 16))
            bioHeight = minimumBioHeight
        }
        
        updateHeight()
    }
    
    
    private func updateHeight() {
        heightConstraint.constant = minimumHeight + bioHeight + bottomSafeAreaConstant
    }
}
