//
//  ChatSettingsModalView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-15.
//

import UIKit

class ChatSettingsModalView: UIView {
    
    static let maxDimmingAlpha : CGFloat = 0.6
    
    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    let modalView = ChatSettingsModalContentView()
    
    private var modalBottomConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : ChatSettingsButtonDelegate!
    
    var modalBottomConstant : CGFloat {
        get { modalBottomConstraint.constant }
        set { modalBottomConstraint.constant = newValue }
    }
    
    var dimmingAlpha : CGFloat {
        get { dimmingView.alpha }
        set { dimmingView.alpha = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(dimmingView)
        self.addSubview(modalView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinView(to: superview)
    }
    
    private func configureViews() {
        dimmingView.backgroundColor = .black
        dimmingView.alpha = 0
    }
    
    private func setUpGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dimmingTapped))
        tap.numberOfTapsRequired = 1
        dimmingView.addGestureRecognizer(tap)
    }
    
    private func makeConstraints() {
        
        modalBottomConstraint = modalView.bottomAnchor.constraint(equalTo: bottomAnchor)
        dimmingView.pinView(to: self)
        
        NSLayoutConstraint.activate([
            modalView.centerXAnchor.constraint(equalTo: centerXAnchor),
            modalBottomConstraint,
        ])
    }
    
    @objc private func dimmingTapped() {
        buttonDelegate.buttonPressed(for: .cancel)
    }
    
}
