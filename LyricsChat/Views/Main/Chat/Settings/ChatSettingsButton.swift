//
//  ChatSettingsButton.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-15.
//

import UIKit

class ChatSettingsButton: BackgroundButton {
    
    static let height = UIScreen.main.bounds.height*0.07

    private let iconView : UIImageView = .preppedForAutoLayout()
    private let customTitleLabel : UILabel = .preppedForAutoLayout()
    
    var icon : UIImage? {
        get { iconView.image }
        set { iconView.image = newValue }
    }
    
    var customTitle : String {
        get { customTitleLabel.text ?? "" }
        set { customTitleLabel.text = newValue }
    }
    
    
    override init() {
        super.init()
        
        configureViews()
        
        self.addSubview(iconView)
        self.addSubview(customTitleLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        highlightedColor = .black.withAlphaComponent(0.05)
        
        iconView.tintColor = .black
        iconView.contentMode = .scaleAspectFit
        
        customTitleLabel.textColor = .black
        customTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.07
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            heightAnchor.constraint(equalToConstant: Self.height),
            
            iconView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3),
            iconView.widthAnchor.constraint(equalTo: iconView.heightAnchor),
            iconView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconView.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            customTitleLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: spacing),
            customTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }

}
