//
//  ChatSettingsModalContentView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-15.
//

import UIKit

class ChatSettingsModalContentView: UIView {

    private let unfriendButton = ChatSettingsButton()
    private let deleteMessagesButton = ChatSettingsButton()
    private let cancelButton : UIButton = .preppedForAutoLayout()
    
    private let minimumHeight : CGFloat
    private let spacing = UIScreen.main.bounds.height*0.02
    
    private var heightConstraint : NSLayoutConstraint!
    private let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
    
    unowned var buttonDelegate : ChatSettingsButtonDelegate!
    
    var height: CGFloat {
        get { heightConstraint.constant }
    }
    
    var bottomSafeAreaConstant : CGFloat {
        get { heightConstraint.constant - minimumHeight }
        set { heightConstraint.constant = minimumHeight + newValue }
    }
    
    
    init() {
        
        let cancelHeight = String.getLabelHeight(text: "", font: buttonFont)
        minimumHeight = ChatSettingsButton.height*2 + spacing*3 + cancelHeight
        
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(unfriendButton)
        self.addSubview(deleteMessagesButton)
        self.addSubview(cancelButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        backgroundColor = .white
        layer.cornerRadius = UIScreen.main.bounds.width*0.055
        layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        unfriendButton.icon = UIImage(systemName: "person.fill.xmark")
        unfriendButton.customTitle = "Unfriend"
        unfriendButton.addTarget(self, action: #selector(unfriendButtonPressed), for: .touchUpInside)
        
        deleteMessagesButton.icon = UIImage(systemName: "trash")
        deleteMessagesButton.customTitle = "Delete All Messages"
        deleteMessagesButton.addTarget(self, action: #selector(deleteMessagesButtonPressed), for: .touchUpInside)
        
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColorPress(.black)
        cancelButton.titleLabel?.font = buttonFont
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        translatesAutoresizingMaskIntoConstraints = false
        heightConstraint = heightAnchor.constraint(equalToConstant: minimumHeight)
        
        NSLayoutConstraint.activate([
            heightConstraint,
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            unfriendButton.topAnchor.constraint(equalTo: topAnchor, constant: spacing),
            unfriendButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            deleteMessagesButton.topAnchor.constraint(equalTo: unfriendButton.bottomAnchor),
            deleteMessagesButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            cancelButton.topAnchor.constraint(equalTo: deleteMessagesButton.bottomAnchor, constant: spacing),
            cancelButton.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    @objc private func unfriendButtonPressed() {
        buttonDelegate.buttonPressed(for: .unfriend)
    }

    @objc private func deleteMessagesButtonPressed() {
        buttonDelegate.buttonPressed(for: .deleteAllMessages)
    }
    
    @objc private func cancelButtonPressed() {
        buttonDelegate.buttonPressed(for: .cancel)
    }
    
}
