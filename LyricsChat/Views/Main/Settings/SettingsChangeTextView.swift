//
//  SettingsChangeTextView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import UIKit

class SettingsChangeTextView: UIView {

    private let headerView : UIView = .viewPreppedForAutoLayout()
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let textField : BottomBorderTextField = .bottomPreppedForAutoLayout()
    private let textViewPlaceholderLabel : UILabel = .preppedForAutoLayout()
    private let textView : UITextView = .preppedForAutoLayout()
    private let textViewBottomLine : UIView = .viewPreppedForAutoLayout()
    private let saveButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    private var saveBottomConstraint : NSLayoutConstraint!
    
    var textViewDelegate : UITextViewDelegate? {
        get { textView.delegate }
        set { textView.delegate = newValue }
    }
    
    var textFieldDelegate : UITextFieldDelegate? {
        get { textField.delegate }
        set { textField.delegate = newValue }
    }
    
    var placeholder : String {
        get { textField.placeholder ?? "" }
        set {
            textField.placeholder = newValue
            textViewPlaceholderLabel.text = newValue
        }
    }
    
    var textViewPlaceholderIsHidden : Bool {
        get { textViewPlaceholderLabel.isHidden }
        set { textViewPlaceholderLabel.isHidden = newValue }
    }
    
    var saveBottomConstant : CGFloat {
        get { saveBottomConstraint.constant }
        set { saveBottomConstraint.constant = newValue }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(headerView)
        self.addSubview(backButton)
        self.addSubview(titleLabel)
        self.addSubview(textField)
        self.addSubview(textView)
        self.addSubview(textViewPlaceholderLabel)
        self.addSubview(textViewBottomLine)
        self.addSubview(saveButton)

        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinViewToSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        saveButton.layer.cornerRadius = saveButton.frame.height/2
    }
    
    private func configureViews() {
        
        headerView.backgroundColor = .white
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.setIconColor(.black)
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        titleLabel.text = "Change Name"
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize:  18, weight: .semibold))
        
        
        let textFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
        
        textViewPlaceholderLabel.textColor = .black
        textViewPlaceholderLabel.font = textFont
        textViewPlaceholderLabel.textAlignment = .center

        textField.textColor = .black
        textField.textAlignment = .center
        textField.font = textFont
        
        textView.textColor = .black
        textView.font = textFont
        textView.isScrollEnabled = false
        
        textViewBottomLine.backgroundColor = .black.withAlphaComponent(0.2)
        
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.baseColor = .customGreen
        saveButton.highlightedColor = .customGreen.withAlphaComponent(0.5)
        saveButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
    }
    
    private func setUpGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard))
        tap.numberOfTapsRequired = 1
        addGestureRecognizer(tap)
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        let topTextSpacing = UIScreen.main.bounds.height*0.04
        
        saveBottomConstraint = saveButton.bottomAnchor.constraint(equalTo: bottomAnchor)
        
        NSLayoutConstraint.activate([
            
            headerView.topAnchor.constraint(equalTo: topAnchor),
            headerView.leftAnchor.constraint(equalTo: leftAnchor),
            headerView.rightAnchor.constraint(equalTo: rightAnchor),
            headerView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.08),
            
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
        
            textField.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: topTextSpacing),
            textField.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            textField.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            textViewPlaceholderLabel.leftAnchor.constraint(equalTo: textView.leftAnchor, constant: 5),
            textViewPlaceholderLabel.topAnchor.constraint(equalTo: textView.topAnchor, constant: textView.textContainerInset.top),
            
            textView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: topTextSpacing),
            textView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            textView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            textViewBottomLine.topAnchor.constraint(equalTo: textView.bottomAnchor),
            textViewBottomLine.heightAnchor.constraint(equalToConstant: 1),
            textViewBottomLine.widthAnchor.constraint(equalTo: textView.widthAnchor),
            textViewBottomLine.centerXAnchor.constraint(equalTo: textField.centerXAnchor),
            
            saveButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.95),
            saveButton.heightAnchor.constraint(equalTo: saveButton.widthAnchor, multiplier: 0.14),
            saveButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            saveBottomConstraint,
        ])
    }
    
    @objc private func closeKeyboard(){
        endEditing(true)
    }

    @objc private func backButtonPressed() {
        print("Back")
    }
    
    @objc private func saveButtonPressed() {
        print("Save")
    }

}


extension SettingsChangeTextView {
    
    func showTextViewHideTextField(_ showTextView : Bool) {
        textField.isHidden = showTextView
        textView.isHidden = !showTextView
        textViewBottomLine.isHidden = !showTextView
        textViewPlaceholderLabel.isHidden = !showTextView
    }
    
}
