//
//  SettingsView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import UIKit

class SettingsView: UIView {

    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let imageView : UIImageView = .preppedForAutoLayout()
    private let changeImageButton : UIButton = .preppedForAutoLayout()
    private let nameSettingButton = SettingsInfoButton()
    private let emailSettingButton = SettingsInfoButton()
    private let bioSettingButton = SettingsInfoButton()
    private let friendsRequestSettingButton = SettingsButton()
    private let signOutButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(backButton)
        self.addSubview(imageView)
        self.addSubview(changeImageButton)
        self.addSubview(nameSettingButton)
        self.addSubview(emailSettingButton)
        self.addSubview(bioSettingButton)
        self.addSubview(friendsRequestSettingButton)
        self.addSubview(signOutButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinViewToSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView.layer.cornerRadius = imageView.frame.height/2
        signOutButton.layer.cornerRadius = signOutButton.frame.height/2
    }
    
    private func configureViews() {
        backgroundColor = .white
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.setIconColor(.black)
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        imageView.image = UIImage(named: "profile1")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        changeImageButton.setTitle("Change Image", for: .normal)
        changeImageButton.setTitleColorPress(.customGreen)
        changeImageButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        
        nameSettingButton.settingTitle = "Name"
        nameSettingButton.info = "Jamal Williams"
        nameSettingButton.addTarget(self, action: #selector(nameButtonPressed), for: .touchUpInside)
        
        emailSettingButton.settingTitle = "Email"
        emailSettingButton.info = "email.address@gmail.com"
        emailSettingButton.addTarget(self, action: #selector(emailButtonPressed), for: .touchUpInside)
        
        bioSettingButton.settingTitle = "Bio"
        bioSettingButton.info = "This is a bio huibo ug biog b8oiug biuggu"
        bioSettingButton.addTarget(self, action: #selector(bioButtonPressed), for: .touchUpInside)
        
        friendsRequestSettingButton.settingTitle = "Friends Request"
        friendsRequestSettingButton.addTarget(self, action: #selector(friendsRequestButtonPressed), for: .touchUpInside)
        
        signOutButton.setTitle("Sign Out", for: .normal)
        signOutButton.setTitleColor(.red, for: .normal)
        signOutButton.highlightedColor = .red.withAlphaComponent(0.05)
        signOutButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        signOutButton.layer.borderWidth = 1
        signOutButton.layer.borderColor = UIColor.red.cgColor
        signOutButton.addTarget(self, action: #selector(signOutButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let topSpacing = UIScreen.main.bounds.height*0.03
        let buttonTopSpacing = UIScreen.main.bounds.height*0.015
        let spacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: topAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            
            imageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.3),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            
            changeImageButton.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: buttonTopSpacing),
            changeImageButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            nameSettingButton.topAnchor.constraint(equalTo: changeImageButton.bottomAnchor, constant: topSpacing),
            nameSettingButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            emailSettingButton.topAnchor.constraint(equalTo: nameSettingButton.bottomAnchor),
            emailSettingButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            bioSettingButton.topAnchor.constraint(equalTo: emailSettingButton.bottomAnchor),
            bioSettingButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            friendsRequestSettingButton.topAnchor.constraint(equalTo: bioSettingButton.bottomAnchor),
            friendsRequestSettingButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            signOutButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            signOutButton.bottomAnchor.constraint(equalTo: bottomAnchor),
            signOutButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            signOutButton.heightAnchor.constraint(equalTo: signOutButton.widthAnchor, multiplier: 0.15),
        ])
    }
    
    @objc private func backButtonPressed() {
        print("Back")
    }
    
    @objc private func nameButtonPressed() {
        print("Name")
    }
    
    @objc private func emailButtonPressed() {
        print("Email")
    }
    
    @objc private func bioButtonPressed() {
        print("Bio")
    }
    
    @objc private func friendsRequestButtonPressed() {
        print("Friends Request")
    }
    
    @objc private func signOutButtonPressed() {
        print("Sign Out")
    }

}
