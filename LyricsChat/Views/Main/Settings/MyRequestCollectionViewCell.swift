//
//  MyRequestCollectionViewCell.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import UIKit

class MyRequestCollectionViewCell: UICollectionViewCell {
    
    static let size = CGSize(width: width, height: height)
    static let id = "MyRequestCollectionViewCell"
    
    private static let width = UIScreen.main.bounds.width
    private static let height = UIScreen.main.bounds.height*0.09
    
    private let imageView : UIImageView = .preppedForAutoLayout()
    private let nameLabel : UILabel = .preppedForAutoLayout()
    private let usernameLabel : UILabel = .preppedForAutoLayout()
    private let deleteButton : IconButton = .iconPreppedForAutoLayout()
    
    private let imageSize = height*0.8
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(imageView)
        self.contentView.addSubview(nameLabel)
        self.contentView.addSubview(usernameLabel)
        self.contentView.addSubview(deleteButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        backgroundColor = .white
        
        imageView.image = UIImage(named: "profile1")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = imageSize/2
        
        nameLabel.text = "Junior Alverado"
        nameLabel.textColor = .black
        nameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        
        usernameLabel.text = "hsoei83"
        usernameLabel.textColor = .darkGray
        usernameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
        
        deleteButton.setImage(UIImage(systemName: "trash"), for: .normal)
        deleteButton.setIconColor(.red)
        deleteButton.imageView?.contentMode = .scaleAspectFit
        deleteButton.pinImageView()
        deleteButton.addTarget(self, action: #selector(deleteButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        let textCenterY = imageSize*0.05
        
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: imageSize),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: spacing),
            imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            nameLabel.bottomAnchor.constraint(equalTo: imageView.centerYAnchor, constant: -textCenterY),
            nameLabel.leftAnchor.constraint(equalTo: imageView.rightAnchor, constant: spacing),
            nameLabel.rightAnchor.constraint(equalTo: deleteButton.leftAnchor, constant: -spacing),

            usernameLabel.topAnchor.constraint(equalTo: imageView.centerYAnchor, constant: textCenterY),
            usernameLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor),
            usernameLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor),
            
            deleteButton.widthAnchor.constraint(equalTo: deleteButton.heightAnchor),
            deleteButton.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.35),
            deleteButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            deleteButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -spacing),
        ])
    }
    
    @objc private func deleteButtonPressed() {
        print("Delete")
    }
    
}
