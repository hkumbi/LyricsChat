//
//  SettingsButton.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import UIKit

class SettingsButton: BackgroundButton {

    private let settingTitleLabel : UILabel = .preppedForAutoLayout()
    private let nextIconView : UIImageView = .preppedForAutoLayout()
    
    var settingTitle : String {
        get { settingTitleLabel.text ?? "" }
        set { settingTitleLabel.text = newValue }
    }
    
    
    override init() {
        super.init()
        
        configureViews()
        
        self.addSubview(settingTitleLabel)
        self.addSubview(nextIconView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        highlightedColor = .black.withAlphaComponent(0.05)
        
        settingTitleLabel.textColor = .black
        settingTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .bold))
        
        nextIconView.image = UIImage(systemName: "chevron.right")
        nextIconView.tintColor = .black
        nextIconView.contentMode = .scaleAspectFit
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height*0.07),
            
            settingTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            settingTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            settingTitleLabel.rightAnchor.constraint(equalTo: nextIconView.leftAnchor, constant: -spacing),
            
            nextIconView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            nextIconView.heightAnchor.constraint(equalTo: nextIconView.heightAnchor),
            nextIconView.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            nextIconView.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }

}
