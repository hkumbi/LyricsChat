//
//  SettingsInfoButton.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import UIKit

class SettingsInfoButton: BackgroundButton {

    private let settingTitleLabel : UILabel = .preppedForAutoLayout()
    private let infoLabel : UILabel = .preppedForAutoLayout()
    private let nextIconView : UIImageView = .preppedForAutoLayout()
    
    var settingTitle : String {
        get { settingTitleLabel.text ?? "" }
        set { settingTitleLabel.text = newValue }
    }
    
    var info : String {
        get { infoLabel.text ?? "" }
        set { infoLabel.text = newValue }
    }
    
    
    override init() {
        super.init()
        
        configureViews()
        
        self.addSubview(settingTitleLabel)
        self.addSubview(infoLabel)
        self.addSubview(nextIconView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        highlightedColor = .black.withAlphaComponent(0.05)
        
        settingTitleLabel.textColor = .black
        settingTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .bold))
        
        infoLabel.textColor = .black
        infoLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
        
        nextIconView.image = UIImage(systemName: "chevron.right")
        nextIconView.tintColor = .black
        nextIconView.contentMode = .scaleAspectFit
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height*0.07),
            
            settingTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            settingTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            settingTitleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.2),
            
            infoLabel.leftAnchor.constraint(equalTo: settingTitleLabel.rightAnchor, constant: spacing),
            infoLabel.rightAnchor.constraint(equalTo: nextIconView.leftAnchor, constant: -spacing),
            infoLabel.centerYAnchor.constraint(equalTo: centerYAnchor),

            nextIconView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            nextIconView.heightAnchor.constraint(equalTo: nextIconView.heightAnchor),
            nextIconView.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            nextIconView.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }

}
