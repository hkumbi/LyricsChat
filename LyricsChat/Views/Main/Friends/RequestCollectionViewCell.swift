//
//  RequestCollectionViewCell.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import UIKit

class RequestCollectionViewCell: UICollectionViewCell {
    
    static let size = CGSize(width: width, height: height)
    static let id = "RequestCollectionViewCell"
    
    private static let width = UIScreen.main.bounds.width
    private static let height = UIScreen.main.bounds.height*0.09
    private static let imageSize = height*0.7
    
    private let profileImageView : UIImageView = .preppedForAutoLayout()
    private let nameLabel : UILabel = .preppedForAutoLayout()
    private let usernameLabel : UILabel = .preppedForAutoLayout()
    private let addButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    private let deleteButton : IconButton = .iconPreppedForAutoLayout()

    private let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 13, weight: .bold))
    
    var cellID : UUID!
    unowned var buttonDelegate : RequestCellButtonDelegate!
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(profileImageView)
        self.contentView.addSubview(nameLabel)
        self.contentView.addSubview(usernameLabel)
        self.contentView.addSubview(addButton)
        self.contentView.addSubview(deleteButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        profileImageView.image = UIImage(named: "profile1")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = Self.imageSize/2
        
        nameLabel.text = "Junior Alverado"
        nameLabel.textColor = .black
        nameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14, weight: .semibold))
        
        usernameLabel.text = "hsoei83"
        usernameLabel.textColor = .darkGray
        usernameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12))
        
        addButton.setTitle("Add", for: .normal)
        addButton.setTitleColor(.white, for: .normal)
        addButton.baseColor = .customGreen
        addButton.highlightedColor = .customGreen.withAlphaComponent(0.5)
        addButton.titleLabel?.font = buttonFont
        addButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)

        deleteButton.setImage(UIImage(systemName: "trash"), for: .normal)
        deleteButton.setIconColor(.red)
        deleteButton.imageView?.contentMode = .scaleAspectFit
        deleteButton.pinImageView()
        deleteButton.addTarget(self, action: #selector(deleteButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        let textCenterY = Self.imageSize*0.1
        let textWidth = 2.5*String.getLabelWidth(text: "Add", font: buttonFont)
        let textHeight = 2*String.getLabelHeight(text: "", font: buttonFont)

        addButton.layer.cornerRadius = textHeight/2
        
        NSLayoutConstraint.activate([
            profileImageView.heightAnchor.constraint(equalToConstant: Self.imageSize),
            profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor),
            profileImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            profileImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: spacing),
            
            nameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: spacing/2),
            nameLabel.rightAnchor.constraint(equalTo: addButton.leftAnchor, constant: -spacing/2),
            nameLabel.bottomAnchor.constraint(equalTo: profileImageView.centerYAnchor, constant: -textCenterY),
            
            usernameLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor),
            usernameLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor),
            usernameLabel.topAnchor.constraint(equalTo: profileImageView.centerYAnchor, constant: textCenterY),
            
            addButton.widthAnchor.constraint(equalToConstant: textWidth),
            addButton.heightAnchor.constraint(equalToConstant: textHeight),
            addButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            addButton.rightAnchor.constraint(equalTo: deleteButton.leftAnchor, constant: -spacing/2),
            
            deleteButton.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.3),
            deleteButton.widthAnchor.constraint(equalTo: deleteButton.heightAnchor),
            deleteButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            deleteButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -spacing/2),
        ])
    }
    
    @objc private func addButtonPressed() {
        buttonDelegate.buttonPressed(for: .add, cellID)
    }
    
    @objc private func deleteButtonPressed() {
        buttonDelegate.buttonPressed(for: .delete, cellID)
    }
    
}
