//
//  FriendsCollectionViewCell.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import UIKit

class FriendsCollectionViewCell: UICollectionViewCell {
    
    static let size = CGSize(width: width, height: height)
    static let id = "FriendsCollectionViewCell"
    
    private static let width = UIScreen.main.bounds.width
    private static let height = UIScreen.main.bounds.height*0.09
    private static let imageSize = height*0.7

    private let profileImageView : UIImageView = .preppedForAutoLayout()
    private let nameLabel : UILabel = .preppedForAutoLayout()
    private let usernameLabel : UILabel = .preppedForAutoLayout()
    private let unfriendButton : BackgroundButton = .backgroundPreppedForAutoLayout()
        
    private let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 13, weight: .bold))

    var cellID : UUID!
    unowned var buttonDelegate : FriendsCellButtonDelegate!

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(profileImageView)
        self.contentView.addSubview(nameLabel)
        self.contentView.addSubview(usernameLabel)
        self.contentView.addSubview(unfriendButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        profileImageView.image = UIImage(named: "profile1")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = Self.imageSize/2
        
        nameLabel.text = "Junior Alverado"
        nameLabel.textColor = .black
        nameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14, weight: .semibold))
        
        usernameLabel.text = "hsoei83"
        usernameLabel.textColor = .darkGray
        usernameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12))
        
        unfriendButton.setTitle("Unfriend", for: .normal)
        unfriendButton.setTitleColor(.red, for: .normal)
        unfriendButton.highlightedColor = .red.withAlphaComponent(0.1)
        unfriendButton.titleLabel?.font = buttonFont
        unfriendButton.layer.borderWidth = 1
        unfriendButton.layer.borderColor = UIColor.red.cgColor
        unfriendButton.addTarget(self, action: #selector(unfriendButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        let textCenterY = Self.imageSize*0.05
        let textWidth = 1.5*String.getLabelWidth(text: "Unfriend", font: buttonFont)
        let textHeight = 2*String.getLabelHeight(text: "", font: buttonFont)
        
        unfriendButton.layer.cornerRadius = textHeight/2
        
        NSLayoutConstraint.activate([
            profileImageView.heightAnchor.constraint(equalToConstant: Self.imageSize),
            profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor),
            profileImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            profileImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: spacing),
            
            nameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: spacing/2),
            nameLabel.rightAnchor.constraint(equalTo: unfriendButton.leftAnchor, constant: -spacing/2),
            nameLabel.bottomAnchor.constraint(equalTo: profileImageView.centerYAnchor, constant: -textCenterY),
            
            usernameLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor),
            usernameLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor),
            usernameLabel.topAnchor.constraint(equalTo: profileImageView.centerYAnchor, constant: textCenterY),
            
            unfriendButton.widthAnchor.constraint(equalToConstant: textWidth),
            unfriendButton.heightAnchor.constraint(equalToConstant: textHeight),
            unfriendButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            unfriendButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -spacing),
        ])
    }
    
    @objc private func unfriendButtonPressed() {
        buttonDelegate.unfriendButtonPressed(cellID)
    }
}
