//
//  FriendsScrollView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import UIKit

class FriendsScrollView: UIScrollView {

    private let contentView : UIView = .viewPreppedForAutoLayout()
    let firstCollection : DefaultCollectionView = .defaultPreppedForAutoLayout()
    let secondCollection : DefaultCollectionView = .defaultPreppedForAutoLayout()
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(contentView)
        self.addSubview(firstCollection)
        self.addSubview(secondCollection)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        isPagingEnabled = true
        showsHorizontalScrollIndicator = false
        
        firstCollection.alwaysBounceVertical = true
        secondCollection.alwaysBounceVertical = true
    }
    
    private func makeConstraints() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: frameLayoutGuide.topAnchor),
            contentView.leftAnchor.constraint(equalTo: contentLayoutGuide.leftAnchor),
            contentView.rightAnchor.constraint(equalTo: contentLayoutGuide.rightAnchor),
            contentView.bottomAnchor.constraint(equalTo: frameLayoutGuide.bottomAnchor),
            
            contentView.centerXAnchor.constraint(equalTo: contentLayoutGuide.centerXAnchor),
            contentView.centerYAnchor.constraint(equalTo: contentLayoutGuide.centerYAnchor),
            
            firstCollection.topAnchor.constraint(equalTo: contentView.topAnchor),
            firstCollection.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            firstCollection.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            firstCollection.widthAnchor.constraint(equalTo: widthAnchor),
            
            secondCollection.topAnchor.constraint(equalTo: contentView.topAnchor),
            secondCollection.leftAnchor.constraint(equalTo: firstCollection.rightAnchor),
            secondCollection.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            secondCollection.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            secondCollection.widthAnchor.constraint(equalTo: widthAnchor),
        ])
    }

}
