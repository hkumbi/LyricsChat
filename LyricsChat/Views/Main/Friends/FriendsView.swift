//
//  FriendsView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import UIKit

class FriendsView: UIView {
    
    private let headerView : UIView = .viewPreppedForAutoLayout()
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let addButton : IconButton = .iconPreppedForAutoLayout()
    private let friendsTabButton : UIButton = .preppedForAutoLayout()
    private let requestTabButton : UIButton = .preppedForAutoLayout()
    private let tabBottomLineView : UIView = .viewPreppedForAutoLayout()
    let scrollView = FriendsScrollView()
    
    let centerX = UIScreen.main.bounds.width*2/5
    
    var tabLineCenterXConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : FriendsViewButtonDelegate!
    
    var lineCenterX : CGFloat {
        get { tabLineCenterXConstraint.constant/centerX }
        set {
            let value = min(max(newValue, 0), 1)
            tabLineCenterXConstraint.constant = value*centerX
        }
    }
    
    var friendsTabColor : UIColor? {
        get { friendsTabButton.titleColor(for: .normal) }
        set { friendsTabButton.setTitleColor(newValue, for: .normal)}
    }
    
    var requestsTabColor : UIColor? {
        get { requestTabButton.titleColor(for: .normal) }
        set { requestTabButton.setTitleColor(newValue, for: .normal)}
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(headerView)
        self.addSubview(backButton)
        self.addSubview(addButton)
        self.addSubview(friendsTabButton)
        self.addSubview(requestTabButton)
        self.addSubview(tabBottomLineView)
        self.addSubview(scrollView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor),
        ])
    }
    
    private func configureViews() {
        
        backgroundColor = .white
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.setIconColor(.black)
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        addButton.setImage(UIImage(systemName: "plus"), for: .normal)
        addButton.setIconColor(.black)
        addButton.imageView?.contentMode = .scaleAspectFit
        addButton.pinImageView()
        addButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
        
        let tabFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        
        friendsTabButton.setTitle("Friends", for: .normal)
        friendsTabButton.setTitleColor(.black, for: .normal)
        friendsTabButton.titleLabel?.font = tabFont
        friendsTabButton.addTarget(self, action: #selector(friendsTabButtonPressed), for: .touchUpInside)
        
        requestTabButton.setTitle("Requests", for: .normal)
        requestTabButton.setTitleColor(.black, for: .normal)
        requestTabButton.titleLabel?.font = tabFont
        requestTabButton.addTarget(self, action: #selector(requestTabButtonPressed), for: .touchUpInside)
        
        tabBottomLineView.backgroundColor = .black
    }

    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        let topSpacing = UIScreen.main.bounds.height*0.01
        
        translatesAutoresizingMaskIntoConstraints = false
        
        tabLineCenterXConstraint = tabBottomLineView.centerXAnchor.constraint(equalTo: friendsTabButton.centerXAnchor)
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: topAnchor),
            headerView.leftAnchor.constraint(equalTo: leftAnchor),
            headerView.rightAnchor.constraint(equalTo: rightAnchor),
            headerView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.07),
            
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            backButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            
            addButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            addButton.heightAnchor.constraint(equalTo: addButton.widthAnchor),
            addButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            addButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            
            friendsTabButton.topAnchor.constraint(equalTo: backButton.bottomAnchor),
            friendsTabButton.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -centerX/2),
            
            requestTabButton.topAnchor.constraint(equalTo: friendsTabButton.topAnchor),
            requestTabButton.centerXAnchor.constraint(equalTo: centerXAnchor, constant: centerX/2),
            
            tabBottomLineView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.25),
            tabBottomLineView.heightAnchor.constraint(equalToConstant: 2),
            tabBottomLineView.topAnchor.constraint(equalTo: friendsTabButton.bottomAnchor),
            tabLineCenterXConstraint,
            
            scrollView.topAnchor.constraint(equalTo: tabBottomLineView.bottomAnchor),
            scrollView.leftAnchor.constraint(equalTo: leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: rightAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.buttonPressed(for: .back)
    }
    
    @objc private func addButtonPressed() {
        buttonDelegate.buttonPressed(for: .add)
    }
    
    @objc private func friendsTabButtonPressed() {
        buttonDelegate.buttonPressed(for: .friendsTab)
    }
    
    @objc private func requestTabButtonPressed() {
        buttonDelegate.buttonPressed(for: .requestsTab)
    }
    
}
