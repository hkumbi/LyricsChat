//
//  NewFriendCollectionViewCell.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-18.
//

import UIKit

class NewFriendCollectionViewCell: UICollectionViewCell {
    
    static let size = CGSize(width: width, height: height)
    static let id = "NewFriendCollectionViewCell"
    
    private static let width = UIScreen.main.bounds.width
    private static let height = UIScreen.main.bounds.height*0.09
    private static let imageSize = height*0.7

    
    private let profileImageView : UIImageView = .preppedForAutoLayout()
    private let nameLabel : UILabel = .preppedForAutoLayout()
    private let usernameLabel : UILabel = .preppedForAutoLayout()
    private let sendRequestButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    private let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 13, weight: .bold))
    
    var id : UUID!
    unowned var buttonDelegate : NewFriendsCellButtonDelegate!

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(profileImageView)
        self.contentView.addSubview(nameLabel)
        self.contentView.addSubview(usernameLabel)
        self.contentView.addSubview(sendRequestButton)
    
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        profileImageView.image = UIImage(named: "profile2")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = Self.imageSize/2
        
        nameLabel.text = "Junior Alverado"
        nameLabel.textColor = .black
        nameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14, weight: .semibold))
        
        usernameLabel.text = "hsoei83"
        usernameLabel.textColor = .darkGray
        usernameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12))
        
        sendRequestButton.setTitle("Send Request", for: .normal)
        sendRequestButton.setTitleColor(.white, for: .normal)
        sendRequestButton.baseColor = .customGreen
        sendRequestButton.highlightedColor = .customGreen.withAlphaComponent(0.5)
        sendRequestButton.titleLabel?.font = buttonFont
        sendRequestButton.addTarget(self, action: #selector(sendRequestButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        let textCenterY = Self.imageSize*0.05
        let textWidth = 1.5*String.getLabelWidth(text: "Send Request", font: buttonFont)
        let textHeight = 2.5*String.getLabelHeight(text: "", font: buttonFont)
        
        sendRequestButton.layer.cornerRadius = textHeight/2

        NSLayoutConstraint.activate([
            profileImageView.heightAnchor.constraint(equalToConstant: Self.imageSize),
            profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor),
            profileImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            profileImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: spacing),
            
            nameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: spacing/2),
            nameLabel.rightAnchor.constraint(equalTo: sendRequestButton.leftAnchor, constant: -spacing/2),
            nameLabel.bottomAnchor.constraint(equalTo: profileImageView.centerYAnchor, constant: -textCenterY),
            
            usernameLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor),
            usernameLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor),
            usernameLabel.topAnchor.constraint(equalTo: profileImageView.centerYAnchor, constant: textCenterY),

            sendRequestButton.widthAnchor.constraint(equalToConstant: textWidth),
            sendRequestButton.heightAnchor.constraint(equalToConstant: textHeight),
            sendRequestButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            sendRequestButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -spacing),
        ])
    }
    
    @objc private func sendRequestButtonPressed() {
        buttonDelegate.sendRequest(id)
    }
    
}
