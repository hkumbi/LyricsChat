//
//  AddNewFriendsView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import UIKit

class AddNewFriendsView: UIView {

    private let headerView : UIView = .viewPreppedForAutoLayout()
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let searchBar : UISearchBar = .preppedForAutoLayout()
    private let cancelButton : UIButton = .preppedForAutoLayout()
    let collectionView : DefaultCollectionView = .defaultPreppedForAutoLayout()
    
    private var cancelLeftConstraint : NSLayoutConstraint!
    
    private let spacing = UIScreen.main.bounds.width*0.03
    
    var searchDelegate : UISearchBarDelegate? {
        get { searchBar.delegate }
        set { searchBar.delegate = newValue }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(headerView)
        self.addSubview(backButton)
        self.addSubview(searchBar)
        self.addSubview(cancelButton)
        self.addSubview(collectionView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor),
        ])
    }
    
    private func configureViews() {
                
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.setIconColor(.black)
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        searchBar.searchBarStyle = .minimal
        
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColorPress(.black)
        cancelButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
    }
    
    private func setUpGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dimmingTapped))
        tap.numberOfTapsRequired = 1
        addGestureRecognizer(tap)
    }
    
    private func makeConstraints() {
        
        translatesAutoresizingMaskIntoConstraints = false
        cancelLeftConstraint = cancelButton.leftAnchor.constraint(equalTo: rightAnchor)
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: topAnchor),
            headerView.leftAnchor.constraint(equalTo: leftAnchor),
            headerView.rightAnchor.constraint(equalTo: rightAnchor),
            headerView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.08),

            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            backButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),

            searchBar.leftAnchor.constraint(equalTo: backButton.rightAnchor, constant: spacing),
            searchBar.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            searchBar.rightAnchor.constraint(equalTo: cancelButton.leftAnchor, constant: -spacing),
            
            cancelButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            cancelLeftConstraint,
            
            collectionView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }

    @objc private func backButtonPressed() {
        print("Back Button Pressed")
    }
    
    @objc private func cancelButtonPressed() {
        print("Cancel Button Pressed")
    }
    
    @objc private func dimmingTapped() {
        endEditing(true)
    }
    
}

extension AddNewFriendsView {
    
    func showCancelButton() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            cancelLeftConstraint.constant = -cancelButton.frame.width - spacing
            layoutIfNeeded()
        }
    }
    
    func hideCancelButton() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            cancelLeftConstraint.constant = 0
            layoutIfNeeded()
        }
    }
    
}
