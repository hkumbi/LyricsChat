//
//  MessageTableViewCell.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-06.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    
    static let height = UIScreen.main.bounds.height*0.09
    
    private let button : BackgroundButton = .backgroundPreppedForAutoLayout()
    private let profileImageView : UIImageView = .preppedForAutoLayout()
    private let nameLabel : UILabel = .preppedForAutoLayout()
    private let recentMessageLabel : UILabel = .preppedForAutoLayout()
    private let iconView : UIImageView = .preppedForAutoLayout()
    private let timeLabel : UILabel = .preppedForAutoLayout()
    
    private let imageSize = height*0.7
    
    private let timeFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12))
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureViews()
        
        self.contentView.addSubview(button)
        self.contentView.addSubview(profileImageView)
        self.contentView.addSubview(nameLabel)
        self.contentView.addSubview(recentMessageLabel)
        self.contentView.addSubview(iconView)
        self.contentView.addSubview(timeLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        button.isEnabled = true
        button.baseColor = .white
        button.highlightedColor = .black.withAlphaComponent(0.1)
        
        profileImageView.image = UIImage(named: "profile2")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = imageSize/2
        
        nameLabel.text = "Josue Rodriguez"
        nameLabel.textColor = .black
        nameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        
        recentMessageLabel.text = "Hello this is a recent message label. Hey this time."
        recentMessageLabel.textColor = .black
        recentMessageLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
        
        iconView.image = UIImage(systemName: "chevron.right")
        iconView.contentMode = .scaleAspectFit
        iconView.tintColor = .black
        
        timeLabel.text = "2d"
        timeLabel.textColor = .black
        timeLabel.font = timeFont
    }
    
    private func makeConstraints() {
        
        let sideSpacing = UIScreen.main.bounds.width*0.05
        let textSpacing : CGFloat = 4
        let timeWidth = String.getLabelWidth(text: "2d", font: timeFont)
        
        button.pinView(to: contentView)
        
        NSLayoutConstraint.activate([
            profileImageView.widthAnchor.constraint(equalToConstant: imageSize),
            profileImageView.heightAnchor.constraint(equalToConstant: imageSize),
            profileImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            profileImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: sideSpacing),
            
            nameLabel.bottomAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -textSpacing),
            nameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: sideSpacing),
            
            recentMessageLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor, constant: textSpacing),
            recentMessageLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: sideSpacing),
            recentMessageLabel.rightAnchor.constraint(equalTo: timeLabel.leftAnchor, constant: -sideSpacing),
            
            timeLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            timeLabel.rightAnchor.constraint(equalTo: iconView.leftAnchor, constant: -3),
            timeLabel.widthAnchor.constraint(equalToConstant: timeWidth),
            
            iconView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.05),
            iconView.heightAnchor.constraint(equalTo: iconView.widthAnchor),
            iconView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -sideSpacing),
        ])
    }
    
}
