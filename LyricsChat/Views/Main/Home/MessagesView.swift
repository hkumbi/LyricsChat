//
//  MessagesView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-06.
//

import UIKit

class MessagesView: UIView {
    
    private let requestsButton : IconButton = .iconPreppedForAutoLayout()
    private let viewTitleLabel : UILabel = .preppedForAutoLayout()
    private let settingsButton : IconButton = .iconPreppedForAutoLayout()
    let tableView : DefaultTableView = .defaultPreppedForAutoLayout()

    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(requestsButton)
        self.addSubview(viewTitleLabel)
        self.addSubview(settingsButton)
        self.addSubview(tableView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor),
        ])
    }
    
    private func configureViews() {
        
        requestsButton.setImage(UIImage(systemName: "person.2"), for: .normal)
        requestsButton.setIconColor(.black)
        requestsButton.imageView?.contentMode = .scaleAspectFit
        requestsButton.pinImageView()
        requestsButton.addTarget(self, action: #selector(requestsButtonPressed), for: .touchUpInside)
        
        viewTitleLabel.text = "Messages"
        viewTitleLabel.textColor = .black
        viewTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .medium))
        
        settingsButton.setImage(UIImage(systemName: "gearshape"), for: .normal)
        settingsButton.setIconColor(.black)
        settingsButton.imageView?.contentMode = .scaleAspectFit
        settingsButton.pinImageView()
        settingsButton.addTarget(self, action: #selector(settingsButtonPressed), for: .touchUpInside)
        
        tableView.separatorStyle = .none
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
        
            requestsButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            requestsButton.heightAnchor.constraint(equalTo: requestsButton.widthAnchor),
            requestsButton.topAnchor.constraint(equalTo: topAnchor),
            requestsButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            viewTitleLabel.centerYAnchor.constraint(equalTo: requestsButton.centerYAnchor),
            viewTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            settingsButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            settingsButton.heightAnchor.constraint(equalTo: settingsButton.widthAnchor),
            settingsButton.topAnchor.constraint(equalTo: topAnchor),
            settingsButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            
            tableView.topAnchor.constraint(equalTo: requestsButton.bottomAnchor, constant: spacing),
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.rightAnchor.constraint(equalTo: rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    @objc private func requestsButtonPressed() {
        
    }
    
    @objc private func settingsButtonPressed() {
        
    }
    
}
