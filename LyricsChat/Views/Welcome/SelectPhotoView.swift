//
//  SelectPhotoView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import UIKit
import PhotosUI


class SelectPhotoView: UIView {
    
    private let cancelButton : UIButton = .preppedForAutoLayout()
    private let saveButton : UIButton = .preppedForAutoLayout()
    private let selectedImageView : UIImageView = .preppedForAutoLayout()
    private let permissionView : UIView = .viewPreppedForAutoLayout()
    private let permissionLabel : UILabel = .preppedForAutoLayout()
    private let permissionButton : UIButton = .preppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let subTitleLabel : UILabel = .preppedForAutoLayout()
    private let allowAccessButton : UIButton = .preppedForAutoLayout()
    
    let collectionView : DefaultCollectionView = .defaultPreppedForAutoLayout()
    
    private var permissionHeightConstraint : NSLayoutConstraint!
    
    private let manageFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .bold))
    private let permissionFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14, weight: .medium))
    
    unowned var buttonDelegate : SelectPhotoButtonDelegate!
    
    var selectedImage : UIImage? {
        get { selectedImageView.image }
        set {
            selectedImageView.image = newValue
            selectedImageView.layer.borderWidth = newValue == nil ? 1 : 0
        }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(cancelButton)
        self.addSubview(saveButton)
        self.addSubview(selectedImageView)
        self.addSubview(permissionView)
        self.addSubview(collectionView)
        self.addSubview(titleLabel)
        self.addSubview(subTitleLabel)
        self.addSubview(allowAccessButton)
        
        self.permissionView.addSubview(permissionLabel)
        self.permissionView.addSubview(permissionButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor)
        ])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        selectedImageView.layer.cornerRadius = selectedImageView.frame.height/2
    }
    
    private func configureViews() {
        
        let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
                
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColorPress(.black)
        cancelButton.titleLabel?.font = buttonFont
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleColorPress(.customGreen)
        saveButton.titleLabel?.font = buttonFont
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
        
        selectedImageView.contentMode = .scaleAspectFill
        selectedImageView.clipsToBounds = true
        selectedImageView.layer.borderWidth = 1
        selectedImageView.layer.borderColor = UIColor.lightGray.cgColor
        
        permissionView.backgroundColor = .black.withAlphaComponent(0.1)
        
        permissionLabel.textColor = .black
        permissionLabel.numberOfLines = 0
        permissionLabel.font = permissionFont
        
        permissionButton.setTitle("Manage", for: .normal)
        permissionButton.setTitleColorPress(.customGreen)
        permissionButton.titleLabel?.font = manageFont
        permissionButton.addTarget(self, action: #selector(permissionButtonPressed), for: .touchUpInside)
        
        titleLabel.text = "Select Profile Image"
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 22, weight: .semibold))
        
        subTitleLabel.textColor = .black
        subTitleLabel.numberOfLines = 0
        subTitleLabel.textAlignment = .center
        subTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
                
        allowAccessButton.setTitle("Allow Access", for: .normal)
        allowAccessButton.setTitleColorPress(.customGreen)
        allowAccessButton.titleLabel?.font = buttonFont
        allowAccessButton.addTarget(self, action: #selector(allowAccessButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let topSpacing = UIScreen.main.bounds.height*0.03
        let sideSpacing = UIScreen.main.bounds.width*0.05
        let buttonWidth = String.getLabelWidth(text: "Manage", font: manageFont)
        let quarterCenterY = UIScreen.main.bounds.height/4
        
        translatesAutoresizingMaskIntoConstraints = false
        
        permissionHeightConstraint = permissionView.heightAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            cancelButton.topAnchor.constraint(equalTo: topAnchor),
            cancelButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            saveButton.topAnchor.constraint(equalTo: topAnchor),
            saveButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
            
            selectedImageView.topAnchor.constraint(equalTo: saveButton.bottomAnchor),
            selectedImageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.25),
            selectedImageView.heightAnchor.constraint(equalTo: selectedImageView.widthAnchor),
            selectedImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            permissionView.topAnchor.constraint(equalTo: selectedImageView.bottomAnchor, constant: topSpacing),
            permissionView.widthAnchor.constraint(equalTo: widthAnchor),
            permissionView.centerXAnchor.constraint(equalTo: centerXAnchor),
            permissionHeightConstraint,
            
            permissionLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            permissionLabel.centerYAnchor.constraint(equalTo: permissionView.centerYAnchor),
            permissionLabel.rightAnchor.constraint(equalTo: permissionButton.leftAnchor, constant: -sideSpacing),
            
            permissionButton.centerYAnchor.constraint(equalTo: permissionView.centerYAnchor),
            permissionButton.widthAnchor.constraint(greaterThanOrEqualToConstant: buttonWidth),
            permissionButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
            
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: centerYAnchor, constant: -quarterCenterY),
            
            subTitleLabel.topAnchor.constraint(equalTo: centerYAnchor, constant: -quarterCenterY+30),
            subTitleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.8),
            subTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            allowAccessButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            allowAccessButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            collectionView.topAnchor.constraint(equalTo: permissionView.bottomAnchor),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }

    @objc private func cancelButtonPressed() {
        buttonDelegate.buttonPressed(for: .cancel)
    }
    
    @objc private func saveButtonPressed() {
        buttonDelegate.buttonPressed(for: .save)
    }
    
    @objc private func permissionButtonPressed() {
        buttonDelegate.buttonPressed(for: .permission)
    }
    
    @objc private func allowAccessButtonPressed() {
        buttonDelegate.buttonPressed(for: .allowAccess)
    }
    
}


extension SelectPhotoView {
    
    func changeViewStatus(_ status : PHAuthorizationStatus) {
        
        isHidden = status == .notDetermined
        allowAccessButton.isHidden = status != .denied
        
        switch status {
        case .notDetermined:
            break
        case .restricted:
            changeViewToDeniedOrRestrictedStatus()
            subTitleLabel.text = "The system has restricted Lyric Chat's access to your photos."
            subTitleLabel.addLineSpacing(4)
            
        case .denied:
            changeViewToDeniedOrRestrictedStatus()
            subTitleLabel.text = "To select an profile image LyricChat you must allow access to your photos"
            subTitleLabel.addLineSpacing(4)
            
        case .authorized:
            changeViewToAuthorizedOrLimitedStatus()
            permissionLabel.text = "LyricChat has access to all your photos"
            updatePermissionViewHeight()
            
        case .limited:
            changeViewToAuthorizedOrLimitedStatus()
            permissionLabel.text = "LyricChat has access to some of your photos"
            updatePermissionViewHeight()

        @unknown default:
            fatalError("Not configured for atatus")
        }
    }
    
    private func changeViewToDeniedOrRestrictedStatus() {
        
        titleLabel.isHidden = false
        subTitleLabel.isHidden = false
        
        saveButton.isHidden = true
        selectedImageView.isHidden = true
        permissionView.isHidden = true
        collectionView.isHidden = true
    }
    
    private func changeViewToAuthorizedOrLimitedStatus() {
        
        saveButton.isHidden = false
        selectedImageView.isHidden = false
        permissionView.isHidden = false
        collectionView.isHidden = false
        
        titleLabel.isHidden = true
        subTitleLabel.isHidden = true
    }
    
    private func updatePermissionViewHeight() {
        
        layoutIfNeeded()
        
        let text = permissionLabel.text ?? ""
        let height = String.getLabelHeight(text: text, font: permissionFont, width: permissionLabel.frame.width)
        let spacing = UIScreen.main.bounds.height*0.04
        
        permissionHeightConstraint.constant = height + spacing
    }
    
}
