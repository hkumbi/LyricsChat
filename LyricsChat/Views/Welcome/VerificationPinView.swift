//
//  VerificationPinView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-03.
//

import UIKit


class VerificationPinView: UIView {
    
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let subTitleLabel : UILabel = .preppedForAutoLayout()
    private let timeLabel : UILabel = .preppedForAutoLayout()
    private let resendCodeButton : UIButton = .preppedForAutoLayout()
    
    let firstDigitTextField : PinTextField = .pinPreppedForAutoLayout()
    let secondDigitTextField : PinTextField = .pinPreppedForAutoLayout()
    let thirdDigitTextField : PinTextField = .pinPreppedForAutoLayout()
    let fourthDigitTextField : PinTextField = .pinPreppedForAutoLayout()
    let fifthDigitTextField : PinTextField = .pinPreppedForAutoLayout()
    let sixthDigitTextField : PinTextField = .pinPreppedForAutoLayout()
    
    unowned var buttonDelegate : VerificationPinButtonDelegate!


    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(backButton)
        self.addSubview(titleLabel)
        self.addSubview(subTitleLabel)
        self.addSubview(firstDigitTextField)
        self.addSubview(secondDigitTextField)
        self.addSubview(thirdDigitTextField)
        self.addSubview(fourthDigitTextField)
        self.addSubview(fifthDigitTextField)
        self.addSubview(sixthDigitTextField)
        self.addSubview(timeLabel)
        self.addSubview(resendCodeButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinViewToSafeArea(superview)
    }
    
    private func configureViews() {
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.setIconColor(.black)
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        titleLabel.text = "Enter Verification Code"
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 24, weight: .semibold))
        
        subTitleLabel.text = "We've sent the code to first.lastname@google.com"
        subTitleLabel.numberOfLines = 0
        subTitleLabel.textColor = .black
        subTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
        
        timeLabel.text = "36s"
        timeLabel.textColor = .black
        timeLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
        
        resendCodeButton.setTitle("Resend Code", for: .normal)
        resendCodeButton.setTitleColorPress(.customGreen)
        resendCodeButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        resendCodeButton.addTarget(self, action: #selector(resendCodeButtonPressed), for: .touchUpInside)
                
        configureDigitTextField(firstDigitTextField)
        configureDigitTextField(secondDigitTextField)
        configureDigitTextField(thirdDigitTextField)
        configureDigitTextField(fourthDigitTextField)
        configureDigitTextField(fifthDigitTextField)
        configureDigitTextField(sixthDigitTextField)
    }
    
    private func configureDigitTextField(_ textField : BottomBorderTextField) {
        
        let digitFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 24))

        textField.font = digitFont
        textField.textColor = .black
        textField.bottomBorderColor = .lightGray
        textField.keyboardType = .numberPad
        textField.textAlignment = .center
    }
    
    private func makeConstraints() {
        
        let topSpacing = UIScreen.main.bounds.height*0.04
        let titleTopSpacing = UIScreen.main.bounds.height*0.02
        let sideSpacing = UIScreen.main.bounds.width*0.05
        let digitWidth = UIScreen.main.bounds.width*0.09
        let digitSpacing = (UIScreen.main.bounds.width - digitWidth*6) / 7
        
        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: topAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: titleTopSpacing),
            titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: titleTopSpacing),
            subTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            firstDigitTextField.topAnchor.constraint(equalTo: subTitleLabel.bottomAnchor, constant: topSpacing),
            firstDigitTextField.leftAnchor.constraint(equalTo: leftAnchor, constant: digitSpacing),
            firstDigitTextField.widthAnchor.constraint(equalToConstant: digitWidth),
            
            secondDigitTextField.topAnchor.constraint(equalTo: firstDigitTextField.topAnchor),
            secondDigitTextField.leftAnchor.constraint(equalTo: firstDigitTextField.rightAnchor, constant: digitSpacing),
            secondDigitTextField.widthAnchor.constraint(equalToConstant: digitWidth),
            
            thirdDigitTextField.topAnchor.constraint(equalTo: firstDigitTextField.topAnchor),
            thirdDigitTextField.widthAnchor.constraint(equalToConstant: digitWidth),
            thirdDigitTextField.leftAnchor.constraint(equalTo: secondDigitTextField.rightAnchor, constant: digitSpacing),
            
            fourthDigitTextField.topAnchor.constraint(equalTo: firstDigitTextField.topAnchor),
            fourthDigitTextField.leftAnchor.constraint(equalTo: thirdDigitTextField.rightAnchor, constant: digitSpacing),
            fourthDigitTextField.widthAnchor.constraint(equalToConstant: digitWidth),
            
            fifthDigitTextField.topAnchor.constraint(equalTo: firstDigitTextField.topAnchor),
            fifthDigitTextField.leftAnchor.constraint(equalTo: fourthDigitTextField.rightAnchor, constant: digitSpacing),
            fifthDigitTextField.widthAnchor.constraint(equalToConstant: digitWidth),
            
            sixthDigitTextField.topAnchor.constraint(equalTo: firstDigitTextField.topAnchor),
            sixthDigitTextField.leftAnchor.constraint(equalTo: fifthDigitTextField.rightAnchor, constant: digitSpacing),
            sixthDigitTextField.widthAnchor.constraint(equalToConstant: digitWidth),
            
            timeLabel.topAnchor.constraint(equalTo: firstDigitTextField.bottomAnchor, constant: titleTopSpacing),
            timeLabel.rightAnchor.constraint(equalTo: resendCodeButton.leftAnchor, constant: -sideSpacing),
            
            resendCodeButton.centerYAnchor.constraint(equalTo: timeLabel.centerYAnchor),
            resendCodeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
        ])
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.backButtonPressed()
    }

    @objc private func resendCodeButtonPressed() {
        buttonDelegate.resendCodeButtonPressed()
    }
    
}


extension VerificationPinView {
    
    func setTextFieldDelegates(_ delegate : UITextFieldDelegate) {
        
        firstDigitTextField.delegate = delegate
        secondDigitTextField.delegate = delegate
        thirdDigitTextField.delegate = delegate
        fourthDigitTextField.delegate = delegate
        fifthDigitTextField.delegate = delegate
        sixthDigitTextField.delegate = delegate
    }
    
    func setPinDelegates(_ delegate : PinTextFieldDelegate) {
        
        firstDigitTextField.pinDelegate = delegate
        secondDigitTextField.pinDelegate = delegate
        thirdDigitTextField.pinDelegate = delegate
        fourthDigitTextField.pinDelegate = delegate
        fifthDigitTextField.pinDelegate = delegate
        sixthDigitTextField.pinDelegate = delegate
    }
    
}
