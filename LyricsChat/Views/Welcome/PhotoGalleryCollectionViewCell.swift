//
//  PhotoGalleryCollectionViewCell.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-06.
//

import UIKit

class PhotoGalleryCollectionViewCell: UICollectionViewCell {
    
    static let size = CGSize(width: width, height: height)
    static let width = Int(UIScreen.main.bounds.width/3 - 1/3)
    static let height = width*5/4
    
    override var isSelected: Bool {
        didSet { cellIsSelected(isSelected) }
    }
    
    private let imageView : UIImageView = .preppedForAutoLayout()
    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    private let checkIconView : UIImageView = .preppedForAutoLayout()
    
    var identiifer : String = ""
    
    var image : UIImage? {
        get { imageView.image }
        set { imageView.image = newValue }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(imageView)
        self.contentView.addSubview(dimmingView)
        self.contentView.addSubview(checkIconView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        cellIsSelected(false)
    }
    
    private func configureViews() {
        
        imageView.image = UIImage(named: "profile1")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        dimmingView.backgroundColor = .black.withAlphaComponent(0.6)
        dimmingView.isHidden = true
        
        checkIconView.image = UIImage(systemName: "checkmark.circle")
        checkIconView.contentMode = .scaleAspectFit
        checkIconView.tintColor = .systemGreen
        checkIconView.isHidden = true
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.02
        
        imageView.pinView(to: contentView)
        dimmingView.pinView(to: contentView)
        
        NSLayoutConstraint.activate([
            checkIconView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.25),
            checkIconView.heightAnchor.constraint(equalTo: checkIconView.widthAnchor),
            checkIconView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: spacing),
            checkIconView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -spacing),
        ])
    }
    
    private func cellIsSelected(_ selected : Bool) {
        
        dimmingView.isHidden = !selected
        checkIconView.isHidden = !selected
    }
    
}
