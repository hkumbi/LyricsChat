//
//  PinTextField.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import UIKit

class PinTextField: BottomBorderTextField {
    
    unowned var pinDelegate : PinTextFieldDelegate!
    
    override init() {
        super.init()
        
        addTarget(self, action: #selector(textDidChange), for: .editingChanged)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func deleteBackward() {
        
        pinDelegate.deleteBackward(self)
        
        super.deleteBackward()
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        
        return super.canPerformAction(action, withSender: sender)
    }

    @objc private func textDidChange() {
        pinDelegate.textDidChange(self)
    }
    
}
