//
//  CompleteProfileView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import UIKit

class CompleteProfileView: UIView {
    
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let skipButton : UIButton = .preppedForAutoLayout()
    private let profileImageView : UIImageView = .preppedForAutoLayout()
    private let noImageLabel : UILabel = .preppedForAutoLayout()
    private let selectPhotoButton : UIButton = .preppedForAutoLayout()
    private let nameTextField : BottomBorderTextField = .bottomPreppedForAutoLayout()
    private let saveButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    unowned var buttonDelegate : CompleteProfileButtonDelegate!


    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(titleLabel)
        self.addSubview(skipButton)
        self.addSubview(profileImageView)
        self.addSubview(noImageLabel)
        self.addSubview(selectPhotoButton)
        self.addSubview(nameTextField)
        self.addSubview(saveButton)

        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinViewToSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        saveButton.layer.cornerRadius = saveButton.frame.height/2
    }
    
    private func configureViews() {
        
        titleLabel.text = "Complete Your Profile"
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
        
        skipButton.setTitle("Skip", for: .normal)
        skipButton.setTitleColorPress(.black)
        skipButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        skipButton.addTarget(self, action: #selector(skipButtonPressed), for: .touchUpInside)
        
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.layer.borderColor = UIColor.lightGray.cgColor
        profileImageView.layer.borderWidth = 1
        
        noImageLabel.text = "No Image"
        noImageLabel.textColor = .darkGray
        noImageLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
        
        selectPhotoButton.setTitle("Select Photo", for: .normal)
        selectPhotoButton.setTitleColorPress(.customGreen)
        selectPhotoButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        selectPhotoButton.addTarget(self, action: #selector(selectPhotoButtonPressed), for: .touchUpInside)

        nameTextField.placeholder = "Enter your name"
        nameTextField.bottomBorderColor = .lightGray
        nameTextField.textAlignment = .center
        nameTextField.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18))
        
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        saveButton.clipsToBounds = true
        saveButton.baseColor = .customGreen
        saveButton.highlightedColor = .customGreen.withAlphaComponent(0.5)
        saveButton.disabledColor = .customGreen
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
    }
    
    private func setUpGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        tap.numberOfTapsRequired = 1
        addGestureRecognizer(tap)
    }
    
    private func makeConstraints() {
        
        let smallTopSpacing = UIScreen.main.bounds.height*0.02
        let topSpacing = UIScreen.main.bounds.height*0.04
        let sideSpacing = UIScreen.main.bounds.width*0.05
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: smallTopSpacing),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            skipButton.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            skipButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
            
            profileImageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.4),
            profileImageView.heightAnchor.constraint(equalTo: profileImageView.widthAnchor),
            profileImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            profileImageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: topSpacing),
            
            noImageLabel.centerXAnchor.constraint(equalTo: profileImageView.centerXAnchor),
            noImageLabel.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor),
            
            selectPhotoButton.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: topSpacing),
            selectPhotoButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            nameTextField.topAnchor.constraint(equalTo: selectPhotoButton.bottomAnchor, constant: topSpacing),
            nameTextField.centerXAnchor.constraint(equalTo: centerXAnchor),
            nameTextField.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.85),
            
            saveButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            saveButton.heightAnchor.constraint(equalTo: saveButton.widthAnchor, multiplier: 0.15),
            saveButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            saveButton.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    @objc private func viewTapped() {
        endEditing(true)
    }
    
    @objc private func skipButtonPressed() {
        buttonDelegate.buttonPressed(for: .skip)
    }

    @objc private func selectPhotoButtonPressed() {
        buttonDelegate.buttonPressed(for: .selectPhoto)
    }
    
    @objc private func saveButtonPressed() {
        buttonDelegate.buttonPressed(for: .save)
    }

}

