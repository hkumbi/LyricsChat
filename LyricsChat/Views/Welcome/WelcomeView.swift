//
//  WelcomeView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-03.
//

import UIKit

class WelcomeView: UIView {
    
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let emailTextField : BottomBorderTextField = .bottomPreppedForAutoLayout()
    private let passwordTextField : BottomBorderTextField = .bottomPreppedForAutoLayout()
    private let signInButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    private let signUpButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    unowned var buttonDelegate : WelcomeButtonDelegate!
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(titleLabel)
        self.addSubview(emailTextField)
        self.addSubview(passwordTextField)
        self.addSubview(signInButton)
        self.addSubview(signUpButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinViewToSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        signInButton.layer.cornerRadius = signInButton.frame.height/2
        signUpButton.layer.cornerRadius = signUpButton.frame.height/2
    }
    
    private func configureViews() {

        titleLabel.text = "Welcome\nBack"
        titleLabel.numberOfLines = 2
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 40, weight: .semibold))
        
        let textFieldFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18))
        
        emailTextField.placeholder = "Enter your email . . ."
        emailTextField.textColor = .black
        emailTextField.font = textFieldFont
        
        passwordTextField.placeholder = "Enter your password . . ."
        passwordTextField.textColor = .black
        passwordTextField.isSecureTextEntry = true
        passwordTextField.font = textFieldFont
        
        let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        
        signInButton.setTitle("Sign In", for: .normal)
        signInButton.setTitleColor(.black, for: .normal)
        signInButton.titleLabel?.font = buttonFont
        signInButton.baseColor = .white
        signInButton.highlightedColor = .lightGray.withAlphaComponent(0.5)
        signInButton.disabledColor = .white
        signInButton.layer.borderWidth = 1
        signInButton.layer.borderColor = UIColor.lightGray.cgColor
        signInButton.addTarget(self, action: #selector(signInButtonPressed), for: .touchUpInside)
        
        signUpButton.setTitle("Sign Up", for: .normal)
        signUpButton.setTitleColor(.white, for: .normal)
        signUpButton.titleLabel?.font = buttonFont
        signUpButton.baseColor = .customGreen
        signUpButton.highlightedColor = .customGreen.withAlphaComponent(0.5)
        signUpButton.disabledColor = .customGreen
        signUpButton.addTarget(self, action: #selector(signUpButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let topSpacing = UIScreen.main.bounds.height*0.05
        let sideSpacing = UIScreen.main.bounds.width*0.1
        let buttonSpacing = UIScreen.main.bounds.height*0.03
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            emailTextField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: topSpacing),
            emailTextField.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.8),
            emailTextField.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: topSpacing),
            passwordTextField.widthAnchor.constraint(equalTo: emailTextField.widthAnchor),
            passwordTextField.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            signInButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            signInButton.heightAnchor.constraint(equalTo: signInButton.widthAnchor, multiplier: 0.15),
            signInButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            signInButton.bottomAnchor.constraint(equalTo: signUpButton.topAnchor, constant: -buttonSpacing),
            
            signUpButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            signUpButton.heightAnchor.constraint(equalTo: signUpButton.widthAnchor, multiplier: 0.15),
            signUpButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            signUpButton.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    @objc private func signInButtonPressed() {
        buttonDelegate.buttonPressed(for: .signIn)
    }
    
    @objc private func signUpButtonPressed() {
        buttonDelegate.buttonPressed(for: .signUp)
    }

}
