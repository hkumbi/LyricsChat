//
//  BackgroundButton.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-03.
//

import UIKit

class BackgroundButton: UIButton {
    
    override var isEnabled: Bool {
        didSet { backgroundColor = isEnabled ? baseColor : disabledColor }
    }
    
    override var isHighlighted: Bool {
        didSet { backgroundColor = isHighlighted ? highlightedColor : baseColor }
    }
    
    var baseColor : UIColor = .white {
        didSet { backgroundColor = baseColor }
    }
    
    var highlightedColor : UIColor = .black.withAlphaComponent(0.4)
    var disabledColor : UIColor = .white
    
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboarrds")
    }

}
