//
//  DefaultCollectionView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-05.
//

import UIKit

class DefaultCollectionView: UICollectionView {

    init() {
        super.init(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view is UIButton {
            return true
        }
        
        return super.touchesShouldCancel(in: view)
    }

}
