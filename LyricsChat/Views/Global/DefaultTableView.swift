//
//  DefaultTableView.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-06.
//

import UIKit

class DefaultTableView: UITableView {

    init() {
        super.init(frame: .zero, style: .plain)
        
        backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        
        if view is UIButton {
            return false
        }
        
        return super.touchesShouldCancel(in: view)
    }

}
