//
//  BottomBorderTextField.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-03.
//

import UIKit


class BottomBorderTextField: UITextField {
    
    override var font: UIFont? {
        didSet { updateHeight() }
    }
    
    private var bottomBorderView : UIView = .viewPreppedForAutoLayout()
    
    private var heightConstraint : NSLayoutConstraint!
    
    private let spacing = UIScreen.main.bounds.height*0.03
    
    var bottomBorderColor : UIColor? {
        get { bottomBorderView.backgroundColor }
        set { bottomBorderView.backgroundColor = newValue }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(bottomBorderView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        bottomBorderView.backgroundColor = .lightGray.withAlphaComponent(0.4)
    }
    
    private func makeConstraints() {
        
        heightConstraint = heightAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            heightConstraint,
            
            bottomBorderView.heightAnchor.constraint(equalToConstant: 1),
            bottomBorderView.widthAnchor.constraint(equalTo: widthAnchor),
            bottomBorderView.centerXAnchor.constraint(equalTo: centerXAnchor),
            bottomBorderView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    private func updateHeight() {

        guard let font = font, heightConstraint != nil else { return }

        let textHeight = String.getLabelHeight(text: "", font: font)
        
        heightConstraint.constant = textHeight + spacing
    }
}
