//
//  ChatModel.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-09.
//

import Foundation


class ChatBaseModel: Hashable, Equatable {
    
    static func == (lhs: ChatBaseModel, rhs: ChatBaseModel) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID()
    var isMyChat : Bool = false
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}


class ChatProfileModel: ChatBaseModel {
    
    
    
}


class ChatModel: ChatBaseModel {
    
    var text = "Hello this is from a chat model. And this instagram reels is so annoying, damn bitch. Shut the fuck up."
    
    override func hash(into hasher: inout Hasher) {
        super.hash(into: &hasher)
        
        hasher.combine(text)
    }
    
}
