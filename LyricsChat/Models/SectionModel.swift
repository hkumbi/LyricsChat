//
//  SectionModel.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-06.
//

import Foundation


enum SectionModel : Hashable, Equatable {
    case main
}
