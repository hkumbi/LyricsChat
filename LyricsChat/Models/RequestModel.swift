//
//  RequestModel.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-16.
//

import Foundation


class RequestModel: Hashable, Equatable {
    
    static func == (lhs: RequestModel, rhs: RequestModel) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID()
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
