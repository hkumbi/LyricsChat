//
//  UserModel.swift
//  LyricsChat
//
//  Created by Herve Kumbi on 2022-09-17.
//

import Foundation


class UserModel: Hashable, Equatable {
    
    static func == (lhs: UserModel, rhs: UserModel) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID()
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
}
